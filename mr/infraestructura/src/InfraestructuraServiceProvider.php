<?php namespace MR\Infraestructura;

use Illuminate\Support\ServiceProvider;

class InfraestructuraServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';

        $this->loadViewsFrom(__DIR__.'/resources/views', 'infraestructura');

        $this->publishes([
			__DIR__ . '/database/migrations/' => base_path('/database/migrations'),
		], 'migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
