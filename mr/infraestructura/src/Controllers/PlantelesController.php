<?php namespace MR\Infraestructura\Controllers;

use H34\Core\Controllers\BaseController;

use MR\Infraestructura\Utilidades\Querys;
use MR\Infraestructura\Models\Plantel;
use MR\Infraestructura\Requests\PlantelRequest;

class PlantelesController extends BaseController
{

    protected $plantel;

    public function __construct(Plantel $plantel)
    {
        $this->plantel = $plantel;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planteles = $this->plantel->all();
        
        return view('infraestructura::planteles.index', compact('planteles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parroquias = Querys::getParroquiasList();

        return view('infraestructura::planteles.create', compact('parroquias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \MR\Requests\PlatelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlantelRequest $request)
    {
        $inputs = $request->input();
        $this->plantel->create($inputs);

        return redirect()->route('planteles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plantel = $this->plantel->findOrFail($id);

        return view('infraestructura::planteles.show', compact('plantel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plantel = $this->plantel->findOrFail($id);
        $parroquias = Querys::getParroquiasList();

        return view('infraestructura::planteles.edit', compact('plantel', 'parroquias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PlantelRequest $request, $id)
    {
        $inputs = $inputs = $request->input();
        $plantel = $this->plantel->findOrFail($id);
        $plantel->update($inputs);

        return redirect()->route('planteles.index');

    }

    public function destroy_confirm($id)
    {
        $plantel = $this->plantel->findOrFail($id);

        return view('infraestructura::planteles.destroy', compact('plantel'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plantel = $this->plantel->findOrFail($id);
        $plantel->delete();

        return redirect()->route('planteles.index');
    }
}
