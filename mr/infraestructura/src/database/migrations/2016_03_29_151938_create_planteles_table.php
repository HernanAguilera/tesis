<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantelesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planteles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',128);
            $table->integer('parroquia_id')->unsigned()->index()->nullable();
            $table->text('ubicacion')->nullable();
            $table->foreign('parroquia_id')->references('id')->on('parroquias')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('planteles');
    }
}
