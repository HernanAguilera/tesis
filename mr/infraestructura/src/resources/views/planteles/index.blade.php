@extends('layouts.admin')

@section('work')
    <h1>Planteles</h1>
    <a class="button small success" href="{{route('planteles.create')}}">
        <i class="step fi-plus size-24"></i>
        Agregar un plantel
    </a>
    @if($planteles->count())
        <table role="grid">
            <thead>
                <tr>
                    <th>
                        Nombre
                    </th>
                    <td>
                        Parroquia
                    </td>
                    <td class="table-actions">

                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($planteles as $key => $plantel)
                    <tr>
                        <td>
                            {{$plantel->nombre}}
                        </td>
                        <td>
                            {{$plantel->parroquia['parroquia']}}
                        </td>
                        <td>
                            <a class="button small secondary action" href="{{route('planteles.show', $plantel->id)}}">
                                <i class="fi-zoom-in"></i>
                                Ver
                            </a>
                            <a class="button small info action" href="{{route('planteles.edit', $plantel->id)}}">
                                <i class="fi-pencil"></i>
                                Editar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert-box">
            No hay planteles registrados
        </div>
    @endif
@stop
