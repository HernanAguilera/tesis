@extends('layouts.admin')

@section('work')
    <h1>Detalle de plantel</h1>
    @include('infraestructura::planteles.partials.detail')
    <a class="button small secondary" href="{{route('planteles.index')}}">
        <i class="fi-list"></i>
        Regresar
    </a>
    <a class="button small info" href="{{route('planteles.edit', $plantel->id)}}">
        <i class="fi-pencil"></i>
        Editar
    </a>
    <a class="button small alert" href="{{route('planteles.destroy.confirm', $plantel->id)}}">
        <i class="fi-trash"></i>
        Eliminar
    </a>
@stop
