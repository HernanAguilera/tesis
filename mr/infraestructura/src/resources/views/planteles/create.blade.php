@extends('layouts.admin')

@section('work')

    <h1>Nuevo plantel</h1>

    {!! Form::open(['method' => 'POST', 'route' => 'planteles.store']) !!}

        {!! Form::label('nombre', 'Nombre') !!}
        {!! Form::text('nombre', NULL, ['class' => 'form-control']) !!}

        {!! Form::label('parroquia_id', 'Parroquia') !!}
        {!! Form::select('parroquia_id', $parroquias ,NULL, ['class' => 'form-control']) !!}

        {!! Form::label('ubicacion', 'Ubicación') !!}
        {!! Form::textarea('ubicacion', NULL, ['class' => 'form-control']) !!}

        <a href="{{route('planteles.index')}}" class="button small secondary">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small success">
            <i class="fi-save"></i>
            Guardar
        </button>

    {!! Form::close() !!}

@stop
