<?php namespace MR\Infraestructura\Models;

use Illuminate\Database\Eloquent\Model;

class Plantel extends Model
{
    protected $table = 'planteles';

    public $timestamps = false;

    protected $fillable = ['nombre', 'parroquia_id', 'ubicacion'];

    public function parroquia(){
        return $this->belongsTo('H34\DivisionTerritorial\Models\Parroquia', 'parroquia_id');
    }
}
