<?php namespace MR\Infraestructura\Utilidades;

use H34\DivisionTerritorial\Models\Parroquia;

class Querys
{

    function __construct(){}

    public static function getParroquias(){
        return Parroquia::where('municipio_id', 18);
    }

    public static function getParroquiasList(){
        return ['' => '----------'] + self::getParroquias()->lists('parroquia', 'id')->toArray();
    }
}
