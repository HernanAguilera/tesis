<?php

use H34\Core\Utilidades\Router;

Route::group(['middleware' => ['web'], 'prefix' => 'infraestructura'], function () {

    // Planteles
    Router::h34resource('MR\Infraestructura\Controllers\PlantelesController', 'planteles');

});
