angular.module('ambiente', ['ngResource', 'ng'])
.constant('HOST', window.location.protocol + '//' + window.location.host)
.constant('CSRF_TOKEN', $('meta[name="csrf-token"]').attr('content'))
;
