angular.module('ambiente')
.controller('ProyectosController', function ($scope, $http, AvanceResource, HOST) {
    var resourceAvance = AvanceResource.create($('#ambiente_id').val());
    var url = HOST + '/academico/ambientes/'+ $('#ambiente_id').val() +'/proyectos-sociolaborales';

    $scope.showFormularioProyecto = false;
    $scope.showFormularioAvance = false;

    $scope.avances = resourceAvance.query();
    $scope.proyecto = {};
    $scope.avance = {};
    $scope.hasProyecto = false;

    $scope.saveProyecto = function () {
        var data = $scope.proyecto;
        if ($scope.hasProyecto === false) {
            $http.post(url + '/store', data)
            .then(function (res) {
                console.log(res);
                $scope.showFormularioProyecto = false;
                $scope.proyecto = res.data.object;
                $scope.hasProyecto = true;
            }, function (errors) {
                console.log(errors)
            })
        }else{
            $http.patch(url + '/update', data)
            .then(function (res) {
                console.log(res.data);
                $scope.showFormularioProyecto = false;
                $scope.proyecto = res.data.object;
            }, function (errors) {
                console.log(errors)
            })
        }
    }

    $scope.getProyecto = function () {
        $http.get(url + '/show', {}).
        then(function (res) {
            console.log(res);
            $scope.proyecto = res.data;

            if ( typeof $scope.proyecto.ambiente_id === 'undefined' ) {
                console.log('ambiente_id  = undefined');
                $scope.hasProyecto = false;
                $scope.proyecto.ambiente_id = $('#ambiente_id').val();
                $scope.saveProyecto();
            }else{
                $scope.hasProyecto = true;
            }

        }, function (error) {
            console.log(error);
            $scope.proyecto = {};
        });

    };
    // Inicializacion
    $scope.getProyecto();

    $scope.showFormProyecto = function () {
        $scope.showFormularioProyecto = true;
    };

    /*
     *  Avances de proyecto
     */

    $scope.showFormAvance = function (avance_id) {
        avance_id = avance_id || null;
        if(avance_id !== null)
            $scope.avance = resourceAvance.get({id:avance_id}, function (data) {
                console.log(data);
                $scope.avance.fecha = moment($scope.avance.fecha, 'YYYY-MM-DD').format('DD/MM/YYYY');
                $scope.showFormularioAvance = true;
            });
    }

    $scope.saveAvance = function () {
        data = $scope.avance;
        data['fecha'] = moment($scope.avance.fecha, 'DD/MM/YYYY').format('YYYY-MM-DD');
        data['proyecto_id'] = $('#ambiente_id').val();
        console.log('enviada', data);

        if( typeof $scope.avance.id !== 'undefined'){
            resourceAvance.update({id:$scope.avance.id}, data, function (data) {
                console.log(data);
                $scope.avance = data.object;
                $scope.avances = data.list;
                $scope.showFormularioAvance = false;
            }, function (error) {
                console.log(error);
            });
        }else {
            resourceAvance.save(data, function (data) {
                console.log(data);
                $scope.avance = data.object;
                $scope.avances = data.list;
                $scope.showFormularioAvance = false;
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.showModalConfirmDeleteAvance = function (avance_id) {
        $scope.avance = resourceAvance.get({id:avance_id});
        $('#confirmDeleteAvance').foundation('reveal', 'open');
    }

    $scope.hideModalConfirmDeleteAvance = function () {
        $('#confirmDeleteAvance').foundation('reveal', 'close');
    }

    $scope.deleteAvance = function (avance_id) {
        resourceAvance.delete({id: avance_id}, function(data){
            console.log(data);
            $scope.avances = data.list;
        });
        $scope.hideModalConfirmDeleteAvance();
        console.log('Eliminar id: ', id);
    }
});
