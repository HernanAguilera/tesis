angular.module('ambiente')
.controller('AsambleasController', function ($scope,
                                            AsambleaResource, DimensionesResource, EvaluacionesResource, AsistenciasResource,
                                            CSRF_TOKEN) {
    resourceAsamblea = AsambleaResource.create($('#ambiente_id').val());
    resourceEvaluaciones = {};

    $scope.showFormulario = false; // En inicio el formulario creación/edición permanece oculto
    $scope.showEvaluacion = false; // En inicio el formulario de evalución permanece oculto
    $scope.showAsistencias = false; // En inicio el formulario de asistencia permanece oculto
    $scope.asambleas = resourceAsamblea.query(); // Se obtiene la lista de asambleas
    $scope.dimensiones = DimensionesResource.query();

    // Inicializa el objeto asamblea y activa la bandera de mostar formulario
    $scope.crearAsamblea = function () {
        $scope.asamblea = {};
        $scope.asamblea.fecha = moment().format('DD/MM/YYYY');
        $scope.showFormulario = true;
    }

    // Salva los datos de la asamblea
    $scope.saveAsamblea = function () {
        data = $scope.asamblea
        data['fecha'] = moment($scope.asamblea.fecha, 'DD/MM/YYYY').format('YYYY-MM-DD');
        data['_token'] = CSRF_TOKEN;
        console.log('enviada', data);

        if( typeof $scope.asamblea.id !== 'undefined'){
            resourceAsamblea.update({id:$scope.asamblea.id}, data, function (data) {
                console.log(data)
                $scope.asambleas = data.list;
                $scope.showFormulario = false;
            }, function (error) {
                console.log(error);
            });
        }else {
            resourceAsamblea.save(data, function (data) {
                console.log(data);
                $scope.asambleas = data.list;
                $scope.showFormulario = false;
            }, function (error) {
                console.log(error);
            });
        }
    }

    // Recupera los datos de la asamblea para ser editados
    $scope.editarAsamblea = function (id) {
        $scope.asamblea = resourceAsamblea.get({id:id}, function () {
            $scope.asamblea.fecha = moment($scope.asamblea.fecha, 'YYYY-MM-DD').format('DD/MM/YYYY');
            $scope.showFormulario = true;
        });
    }

    // Modal de confirmación para eliminación de asamblea
    $scope.showModal = function (id) {
        $scope.asamblea = resourceAsamblea.get({id:id});
        $('#confirmDeleteAsamblea').foundation('reveal', 'open');
    }

    // Oculta el modal de confirmación para eliminación de asamblea
    $scope.hideModal = function () {
        $('#confirmDeleteAsamblea').foundation('reveal', 'close');
    }

    // Elimina la asamblea
    $scope.eliminarAsamblea = function (id) {
        resourceAsamblea.delete({id: id}, function(data){
            console.log(data);
            $scope.asambleas = data.list;
        });
        $scope.hideModal();
        console.log('Eliminar id: ', id);
    }



    /**
     * ACA INICIA LA FUNCIONALIDAD DE EVALUACION
     */

    $scope.showEvaluar = function (asamblea_id) {
        $scope.showEvaluacion = true;
        // console.log(id);
        $scope.asamblea = {id:asamblea_id};
        resourceEvaluaciones = EvaluacionesResource.create($('#ambiente_id').val(), asamblea_id);
        resourceEvaluaciones.query(function (data) {
            for (vencedor of data) {
                var temp = [];
                for (dimension of $scope.dimensiones) {
                    evaluacion = $scope.getEvaluacion(vencedor.evaluaciones, dimension.id)
                    if(typeof evaluacion.id === 'undefined')
                        evaluacion.dimension_id = dimension.id;
                    temp.push(evaluacion);
                }
                vencedor.evaluaciones = temp;
            }
            $scope.vencedores = data; // retorna vencedores con las evaluaciones para esta asamblea
        });
    }

    $scope.getEvaluacion = function (evaluaciones, dimension_id) {
        for (evaluacion of evaluaciones) {
            if (evaluacion.dimension_id == dimension_id)
                return evaluacion;
        }
        return {};
    }

    $scope.getDimension = function (id) {
        for (dimension of $scope.dimensiones) {
            if(dimension.id == id)
                return dimension;
        }
        return {descripcion:null};
    }

    $scope.cancelarEvaluar = function () {
        $scope.showEvaluacion = false;
        $scope.asamblea = {};
    }

    $scope.evaluar = function () {
        $scope.showEvaluacion = false;

        for (vencedor of $scope.vencedores) {
            for (evaluacion of vencedor.evaluaciones) {
                evaluacion.vencedor_id = vencedor.id;
                evaluacion.actividad_academica_id = $scope.asamblea.id;

                if (typeof evaluacion.id !== 'undefined') {
                    resourceEvaluaciones.update({id:evaluacion.id}, evaluacion, function (data) {
                        console.log(data);
                    }, function (error) {
                        console.log(error)
                    });
                }else {
                    if(typeof evaluacion.valoracion !== 'undefined'){
                        resourceEvaluaciones.save(evaluacion, function (data) {
                            console.log(data);
                        }, function (error) {
                            console.log(error);
                        })
                    }
                }
            }
        }

        $scope.asamblea = {};
    }

    /**
     * ACA INICIA LA FUNCIONALIDAD DE ASISTENCIA
     */

     $scope.showAsignarAsistencias = function (asamblea_id) {
         $scope.showAsistencias = true;
         $scope.asamblea = {id:asamblea_id};
         resourceAsistencia = AsistenciasResource.create($('#ambiente_id').val(), asamblea_id);
         resourceAsistencia.query(function (data) {
             $scope.asistencias = data;
             $scope.asistencias.todos = false;
         });
     }

     $scope.cancelarAsistencias = function () {
         $scope.showAsistencias = false;
         $scope.asamblea = {};
     }

     $scope.almacenarAsistencia = function () {
         $scope.showAsistencias = false;

         for (vencedor of $scope.asistencias) {
             if (vencedor.asistencia !== null){
                 vencedor.asistencia.vencedor_id = vencedor.id;
                 if (typeof vencedor.asistencia.id !== 'undefined') {
                     resourceAsistencia.update({id:vencedor.asistencia.id}, vencedor.asistencia, function (data) {
                         console.log(data);
                     }, function (error) {
                         console.log(error);
                     })
                 }else{
                     if (typeof vencedor.asistencia.asistio !== 'undefined') {
                         console.log(vencedor.asistencia);
                         resourceAsistencia.save(vencedor.asistencia, function (data) {
                             console.log(data);
                         }, function (error) {
                             console.log(error);
                         })
                     }
                 }
             }
         }

         $scope.asamblea = {};
     }

     $scope.asistieronTodos = function () {
         var cambio = $scope.asistencias.todos ? '1' : '0';
         for (vencedor of $scope.asistencias) {
             if (vencedor.asistencia === null)
                vencedor.asistencia = {asistio: cambio};
            else {
                vencedor.asistencia.asistio = cambio;
            }
         }
     }
});
