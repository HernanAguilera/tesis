<?php namespace MR\Academico\Requests;

use H34\Core\Requests\Request;

class AmbienteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plantel_id' => 'exists:planteles,id',
            'cohorte_id' => 'exists:cohortes,id',
            'semestre_id' => 'exists:semestres,id',
        ];
    }
}
