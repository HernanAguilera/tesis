<?php namespace MR\Academico\Requests;

use H34\Core\Requests\Request;

class VisualizacionVideoclaseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion' => 'alpha_es'
        ];
    }
}
