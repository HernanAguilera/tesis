<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class Asamblea extends Model
{
    protected $table = 'asambleas';

    public $timestamps = false;

    protected $primaryKey = 'actividad_academica_id';

    protected $fillable = ['actividad_academica_id'];

    public function actividadAcademica(){
        return $this->belongsTo('\MR\Academico\Models\ActividadAcademica', 'id');
    }
}
