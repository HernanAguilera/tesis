<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class Avance extends Model
{
    protected $fillable = ['descripcion', 'fecha', 'proyecto_id'];

    public function proyecto(){
        return $this->belongsTo('\MR\Academico\Models\ProyectoSociolaboral');
    }
}
