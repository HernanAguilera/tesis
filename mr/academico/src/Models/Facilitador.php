<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class Facilitador extends Model
{
    protected $table = 'facilitadores';

    protected $fillable = ['nombres', 'apellidos', 'cedula', 'fecha_nacimiento'];

    public function ambientes(){
        return $this->belongsToMany('\MR\Academico\Models\Ambiente', 'ambiente_facilitador', 'facilitador_id', 'ambiente_id')
                    ->withPivot('activo', 'fecha_inicio', 'fecha_fin');
    }
}
