<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class ActividadAcademica extends Model
{
    protected $table = 'actividades_academicas';

    public $timestamps = false;

    protected $fillable = ['descripcion', 'fecha', 'ambiente_id'];

    public function ambiente(){
        return $this->belongsTo('\MR\Academico\Models\Ambiente');
    }

    public function asamblea(){
        return $this->hasOne('\MR\Academico\Models\Asamblea', 'actividad_academica_id');
    }

    public function visualizacionVideo(){
        return $this->hasOne('\MR\Academico\Models\VisualizacionVideoclase', 'actividad_academica_id');
    }

    public function complementaria(){
        return $this->hasOne('\MR\Academico\Models\Complementaria', 'actividad_academica_id');
    }

    public function evaluaciones(){
        return $this->hasMany('\MR\Academico\Models\Evaluacion');
    }

    public function evaluacionesWithVencedores(){
        $vencedores = $this->ambiente->vencedoresActivos()->select('id', 'nombres', 'apellidos', 'cedula')->get();

        foreach ($vencedores as $vencedor) {
            $vencedor->evaluaciones = $vencedor->evaluaciones()->where('actividad_academica_id', $this->id)
                                                               ->select('id', 'dimension_id', 'valoracion')->get();
        }

        return $vencedores;
    }

    public function asistencias(){
        return $this->hasMany('\MR\Academico\Models\Asistencia');
    }

    public function asistenciasWithVencedores(){
        $vencedores = $this->ambiente->vencedoresActivos()->select('id', 'nombres', 'apellidos', 'cedula')->get();

        foreach ($vencedores as $vencedor) {
            $vencedor->asistencia = $vencedor->asistencias()->where('actividad_academica_id', $this->id)
                                                            ->select('id', 'asistio')->first();
        }

        return $vencedores;
    }
}
