<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    protected $fillable = ['asistio', 'actividad_academica_id', 'vencedor_id'];

    public function actividadAcademica(){
        return $this->belongsTo('\MR\Academico\Models\actividadAcademica', 'actividad_academica_id');
    }

    public function vencedor(){
        return $this->belongsTo('\MR\Academico\Models\Vencedor');
    }
}
