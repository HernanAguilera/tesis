<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class VideoClase extends Model
{
    protected $table = 'videoclases';

    protected $fillable = ['descripcion', 'asignatura_id'];

    public function asignatura(){
        return $this->belongsTo('\MR\Academico\Models\Asignatura');
    }
}
