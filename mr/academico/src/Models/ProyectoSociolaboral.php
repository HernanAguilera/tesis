<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class ProyectoSociolaboral extends Model
{
    protected $table = 'proyectos_sociolaborales';

    protected $primaryKey = 'ambiente_id';

    protected $fillable = ['titulo', 'ambiente_id'];

    public function ambiente(){
        return $this->belongsTo('\MR\Academico\Models\Ambiente', 'id');
    }

    public function avances(){
        return $this->hasMany('\MR\Academico\Models\Avance', 'proyecto_id');
    }
}
