<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class VisualizacionVideoclase extends Model
{
    protected $table = 'visualizaciones_videoclases';

    public $timestamps = false;

    protected $primaryKey = 'actividad_academica_id';

    protected $fillable = ['actividad_academica_id', 'videoclase_id'];

    public function actividadAcademica(){
        return $this->belongsTo('\MR\Academico\Models\ActividadAcademica', 'id');
    }
}
