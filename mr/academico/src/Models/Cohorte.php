<?php  namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class Cohorte extends Model
{
    protected $table = 'cohortes';

    protected $fillable = ['descripcion'];
}
