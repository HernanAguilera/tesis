<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class Complementaria extends Model
{
    protected $table = 'complementarias';

    public $timestamps = false;

    protected $primaryKey = 'actividad_academica_id';

    protected $fillable = ['actividad_academica_id', 'tipo'];

    public function actividadAcademica(){
        return $this->belongsTo('\MR\Academico\Models\ActividadAcademica', 'id');
    }
}
