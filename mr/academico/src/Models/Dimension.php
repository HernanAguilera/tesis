<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class Dimension extends Model
{
    protected $table = 'dimensiones';

    protected $fillable = ['descripcion'];
}
