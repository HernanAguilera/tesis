<?php  namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluacion extends Model
{
    protected $table = 'evaluaciones';

    protected $fillable = ['valoracion', 'actividad_academica_id', 'vencedor_id', 'dimension_id'];

    public function actividadAcademica(){
        return $this->belongsTo('\MR\Academico\Models\actividadAcademica', 'actividad_academica_id');
    }

    public function vencedor(){
        return $this->belongsTo('\MR\Academico\Models\Vencedor');
    }

    public function dimension(){
        return $this->belongsTo('\MR\Academico\Models\Dimension');
    }
}
