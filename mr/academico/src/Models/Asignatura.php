<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class Asignatura extends Model
{
    protected $table = 'asignaturas';

    protected $fillable = ['descripcion', 'semestre_id'];

    public function semestre(){
        return $this->belongsTo('\MR\Academico\Models\Semestre');
    }
}
