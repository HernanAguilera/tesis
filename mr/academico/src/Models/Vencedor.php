<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class Vencedor extends Model
{
    protected $table = 'vencedores';

    public $timestamps = false;

    protected $fillable = ['nombres', 'apellidos', 'cedula', 'fecha_nacimiento'];

    public function ambientes(){
        return $this->belongsToMany('\MR\Academico\Models\Ambiente', 'ambiente_vencedor', 'vencedor_id', 'ambiente_id')
                    ->withPivot('activo', 'fecha_incorporacion', 'fecha_desincorporacion');
    }

    public function evaluaciones(){
        return $this->hasMany('\MR\Academico\Models\Evaluacion');
    }

    public function asistencias(){
        return $this->hasMany('\MR\Academico\Models\Asistencia');
    }
}
