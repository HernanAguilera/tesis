<?php namespace MR\Academico\Models;

use Illuminate\Database\Eloquent\Model;

class Ambiente extends Model
{
    protected $table = 'ambientes';

    protected $fillable = ['nombre', 'plantel_id', 'cohorte_id', 'semestre_id'];

    public function plantel(){
        return $this->belongsTo('\MR\Infraestructura\Models\Plantel');
    }

    public function cohorte(){
        return $this->belongsTo('\MR\Academico\Models\Cohorte');
    }

    public function semestre(){
        return $this->belongsTo('\MR\Academico\Models\Semestre');
    }

    public function facilitadores(){
        return $this->belongsToMany('\MR\Academico\Models\Facilitador', 'ambiente_facilitador', 'ambiente_id', 'facilitador_id');
    }

    public function actividadesAcademicas(){
        return $this->hasMany('\MR\Academico\Models\ActividadAcademica');
    }


    /*
     *  Proyecto socio-productivo y avances del mismo
     */
     public function proyectoSociolaboral(){
         return $this->hasOne('\MR\Academico\Models\ProyectoSociolaboral', 'ambiente_id');
     }

    public function hasProyecto(){
        if ( $this->proyectoSociolaboral()->first() )
            return true;
        return false;
    }

    public function avancesProyecto(){
        if( $this->hasProyecto() )
            return $this->proyectoSociolaboral()->first()->avances()->get();
        return [];
    }

    public function findAvanceProyecto($id){
        $avance = $this->hasProyecto() ? $this->proyectoSociolaboral()->first()->avances()->where('id', $id)->first() : null;
        return $avance;
    }

    /*
     *  Vencedores
     */
    public function vencedores(){
        return $this->belongsToMany('\MR\Academico\Models\Vencedor', 'ambiente_vencedor', 'ambiente_id', 'vencedor_id');
    }

    public function vencedoresWithPivot(){
        return $this->vencedores()->withPivot('activo', 'fecha_incorporacion', 'fecha_desincorporacion');
    }

    public function vencedoresActivos(){
        return $this->vencedoresWithPivot()->where('activo', '1');
    }

    /**
     *  Asambleas
     */
    public function asambleas(){
        return Asamblea::join('actividades_academicas', 'asambleas.actividad_academica_id', '=', 'actividades_academicas.id')
                        ->join('ambientes', 'actividades_academicas.ambiente_id', '=', 'ambientes.id')
                        ->where('ambientes.id', $this->id)
                        ->select('actividades_academicas.*')
                        ;
    }

    public function createAsamblea($datos){
        return $this->actividadesAcademicas()
             ->create($datos)
             ->asamblea()
             ->create($datos)
             ;
    }

    public function findAsamblea($id){
        return $this->asambleas()
                    ->where('asambleas.actividad_academica_id', $id)
                    ->first()
                    ;
    }

    /**
     *  Visualizaciones de videos
     */
    public function visualizacionesVideos(){
        return VisualizacionVideoclase::join('actividades_academicas', 'visualizaciones_videoclases.actividad_academica_id', '=', 'actividades_academicas.id')
                                        ->join('videoclases', 'videoclases.id', '=', 'visualizaciones_videoclases.videoclase_id')
                                        ->join('ambientes', 'actividades_academicas.ambiente_id', '=', 'ambientes.id')
                                        ->where('ambientes.id', $this->id)
                                        ->select('actividades_academicas.*', 'visualizaciones_videoclases.*', 'videoclases.asignatura_id')
                        ;
    }

    public function createVisualizacionVideoclase($datos){
        return $this->actividadesAcademicas()
             ->create($datos)
             ->visualizacionVideo()
             ->create($datos)
             ;
    }

    public function findVisualizacionVideoclase($id){
        return $this->visualizacionesVideos()
                    ->where('visualizaciones_videoclases.actividad_academica_id', $id)
                    ->first()
                    ;
    }

    /**
     *  Actividades academicas complementarias
     */
    public function complementarias(){
        return Complementaria::join('actividades_academicas', 'complementarias.actividad_academica_id', '=', 'actividades_academicas.id')
                            ->join('ambientes', 'actividades_academicas.ambiente_id', '=', 'ambientes.id')
                            ->where('ambientes.id', $this->id)
                            ->select('actividades_academicas.*', 'complementarias.*')
                        ;
    }

    public function createComplementaria($datos){
        return $this->actividadesAcademicas()
             ->create($datos)
             ->complementaria()
             ->create($datos)
             ;
    }

    public function findComplementaria($id){
        return $this->complementarias()
                    ->where('complementarias.actividad_academica_id', $id)
                    ->first()
                    ;
    }
}
