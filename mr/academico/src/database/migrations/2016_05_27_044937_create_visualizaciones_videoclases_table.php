<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisualizacionesVideoclasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visualizaciones_videoclases', function (Blueprint $table) {
            $table->integer('actividad_academica_id')->unsigned()->index();
            $table->integer('videoclase_id')->unsigned()->index()->nullable();
            $table->foreign('actividad_academica_id')->references('id')->on('actividades_academicas')->onDelete('cascade');
            $table->foreign('videoclase_id')->references('id')->on('videoclases')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visualizaciones_videoclases');
    }
}
