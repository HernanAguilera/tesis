<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotAmbienteVencedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambiente_vencedor', function (Blueprint $table) {
            $table->integer('ambiente_id')->unsigned()->index();
			$table->foreign('ambiente_id')->references('id')->on('ambientes')->onDelete('cascade');
			$table->integer('vencedor_id')->unsigned()->index();
			$table->foreign('vencedor_id')->references('id')->on('vencedores')->onDelete('cascade');
            $table->boolean('activo');
            $table->date('fecha_incorporacion')->nullable();
            $table->date('fecha_desincorporacion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ambiente_vencedor');
    }
}
