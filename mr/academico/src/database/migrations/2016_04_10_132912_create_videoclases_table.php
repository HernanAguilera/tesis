<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoclasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videoclases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 128)->nullable();
            $table->integer('asignatura_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->foreign('asignatura_id')->references('id')->on('asignaturas')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videoclases');
    }
}
