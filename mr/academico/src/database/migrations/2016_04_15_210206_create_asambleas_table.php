<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsambleasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asambleas', function (Blueprint $table) {
            $table->integer('actividad_academica_id')->unsigned()->index();
            $table->foreign('actividad_academica_id')->references('id')->on('actividades_academicas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asambleas');
    }
}
