<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencias', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('asistio')->nullable();
            $table->integer('actividad_academica_id')->unsigned()->index();
            $table->integer('vencedor_id')->unsigned()->index();
            $table->foreign('actividad_academica_id')->references('id')->on('actividades_academicas')->onDelete('cascade');
            $table->foreign('vencedor_id')->references('id')->on('vencedores')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asistencias');
    }
}
