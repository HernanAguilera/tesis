<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('valoracion', 32)->nullable();
            $table->integer('actividad_academica_id')->unsigned()->index();
            $table->integer('vencedor_id')->unsigned()->index();
            $table->integer('dimension_id')->unsigned()->index()->nullable();
            $table->foreign('actividad_academica_id')->references('id')->on('actividades_academicas')->onDelete('cascade');
            $table->foreign('vencedor_id')->references('id')->on('vencedores')->onDelete('cascade');
            $table->foreign('dimension_id')->references('id')->on('dimensiones')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evaluaciones');
    }
}
