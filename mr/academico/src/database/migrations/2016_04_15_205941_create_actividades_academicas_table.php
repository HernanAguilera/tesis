<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadesAcademicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividades_academicas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 128)->nullable();
            $table->date('fecha')->nullable();
            $table->integer('ambiente_id')->unsigned()->index();
            $table->foreign('ambiente_id')->references('id')->on('ambientes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actividades_academicas');
    }
}
