<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 128)->nullable();
            $table->integer('plantel_id')->unsigned()->index()->nullable();
            $table->integer('cohorte_id')->unsigned()->index()->nullable();
            $table->integer('semestre_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->foreign('plantel_id')->references('id')->on('planteles')->onDelete('set null');
            $table->foreign('cohorte_id')->references('id')->on('cohortes');
            $table->foreign('semestre_id')->references('id')->on('semestres');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ambientes');
    }
}
