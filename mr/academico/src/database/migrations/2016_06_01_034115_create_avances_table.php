<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('proyecto_id')->unsigned()->index();
            $table->date('fecha')->nullable();
            $table->text('descripcion')->nullable();
            $table->timestamps();
            $table->foreign('proyecto_id')->references('ambiente_id')->on('proyectos_sociolaborales')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('avances');
    }
}
