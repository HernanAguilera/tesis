<?php namespace MR\Academico\Controllers\api;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\DimensionRequest as Request;
use MR\Academico\Models\Dimension;

class DimensionesController extends Controller
{

    protected $dimension;

    public function __construct(Dimension $dimension)
    {
        $this->dimension = $dimension;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dimensiones = $this->dimension->all();

        return response()->json($dimensiones);

        // return view('academico::dimensiones.index', compact('dimensiones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('academico::dimensiones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        $this->dimension->create($inputs);

        return redirect()->route('dimensiones.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dimension = $this->dimension->findOrFail($id);

        return view('academico::dimensiones.show', compact('dimension'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dimension = $this->dimension->findOrFail($id);

        return view('academico::dimensiones.edit', compact('dimension'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->input();
        $dimension = $this->dimension->findOrFail($id);
        $dimension->update($inputs);

        return redirect()->route('dimensiones.index');
    }

    public function destroy_confirm($id)
    {
        $dimension = $this->dimension->findOrFail($id);

        return view('academico::dimensiones.destroy', compact('dimension'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dimension = $this->dimension->findOrFail($id);
        $dimension->delete();

        return redirect()->route('dimensiones.index');
    }
}
