<?php namespace MR\Academico\Controllers\api;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\EvaluacionRequest as Request;
use MR\Academico\Models\Ambiente;
use MR\Academico\Models\ActividadAcademica;
use MR\Academico\Models\Evaluacion;

class EvaluacionesController extends Controller
{
    protected $ambiente;
    protected $evaluacion;
    protected $actividad_academica;

    public function __construct(ActividadAcademica $actividad_academica, Ambiente $ambiente, Evaluacion $evaluacion){
        $this->actividad_academica = $actividad_academica;
        $this->ambiente = $ambiente;
        $this->evaluacion = $evaluacion;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ambiente_id, $actividad_academica_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $actividad_academica = $this->actividad_academica->findOrFail($actividad_academica_id);
        // $evaluaciones = $actividad_academica->evaluaciones()->get();
        $evaluaciones = $actividad_academica->evaluacionesWithVencedores();
        return response()->json($evaluaciones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($ambiente_id, $actividad_academica_id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($ambiente_id, $actividad_academica_id, Request $request)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $actividad_academica = $this->actividad_academica->findOrFail($actividad_academica_id);
        $inputs = $request->input();
        $evaluacion = $actividad_academica->evaluaciones()->create($inputs);

        $evaluaciones = $actividad_academica->evaluacionesWithVencedores();
        return response()->json(['message' => 'Evaluacion almacenada', 'list' => $evaluaciones, 'object' => $evaluacion]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ambiente_id, $actividad_academica_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $actividad_academica = $this->actividad_academica->findOrFail($actividad_academica_id);
        $evaluacion = $actividad_academica->evaluaciones()->where('id', $id)->first();

        return response()->json($evaluacion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ambiente_id, $actividad_academica_id, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($ambiente_id, $actividad_academica_id, Request $request, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $actividad_academica = $this->actividad_academica->findOrFail($actividad_academica_id);
        $evaluacion = $actividad_academica->evaluaciones()->where('id', $id)->first();
        $inputs = $request->input();

        $evaluacion->update($inputs);

        $evaluaciones = $actividad_academica->evaluacionesWithVencedores();
        return response()->json(['message' => 'Evaluacion almacenada', 'list' => $evaluaciones, 'object' => $evaluacion]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ambiente_id, $actividad_academica_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $actividad_academica = $this->actividad_academica->findOrFail($actividad_academica_id);
        $evaluacion = $actividad_academica->evaluaciones()->where('id', $id)->first();

        $evaluacion->delete();

        $evaluaciones = $actividad_academica->evaluacionesWithVencedores();
        return response()->json(['message' => 'Evaluacion eliminada', 'list' => $evaluaciones]);
    }
}
