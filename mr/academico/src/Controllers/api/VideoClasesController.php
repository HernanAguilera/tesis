<?php namespace MR\Academico\Controllers\api;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\VideoclaseRequest as Request;
use MR\Academico\Models\Asignatura;
use MR\Academico\Models\VideoClase;

class VideoClasesController extends Controller
{

    protected $videoclase;
    protected $asignatura;

    public function __construct(VideoClase $videoclase, Asignatura $asignatura)
    {
        $this->videoclase = $videoclase;
        $this->asignatura = $asignatura;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videoclases = $this->videoclase->all();

        return response()->json($videoclases);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $asignaturas = $this->asignatura->lists('descripcion', 'id');

        return view('academico::videoclases.create', compact('asignaturas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        $this->videoclase->create($inputs);

        return redirect()->route('videoclases.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $videoclase = $this->videoclase->findOrFail($id);

        return view('academico::videoclases.show', compact('videoclase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $videoclase = $this->videoclase->findOrFail($id);
        $asignaturas = $this->asignatura->lists('descripcion', 'id');

        return view('academico::videoclases.edit', compact('videoclase', 'asignaturas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->input();
        $videoclase = $this->videoclase->findOrFail($id);
        $videoclase->update($inputs);

        return redirect()->route('videoclases.index');
    }


    public function destroy_confirm($id)
    {
        $videoclase = $this->videoclase->findOrFail($id);

        return view('academico::videoclases.destroy', compact('videoclase'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $videoclase = $this->videoclase->findOrFail($id);
        $videoclase->delete();

        return redirect()->route('videoclases.index');
    }
}
