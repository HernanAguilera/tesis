<?php namespace MR\Academico\Controllers\api;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\AsistenciaRequest as Request;
use MR\Academico\Models\Ambiente;
use MR\Academico\Models\ActividadAcademica;
use MR\Academico\Models\Asistencia;

class AsistenciasController extends Controller
{

    protected $ambiente;
    protected $asistencia;
    protected $actividad_academica;

    public function __construct(ActividadAcademica $actividad_academica, Ambiente $ambiente, Asistencia $asistencia){
        $this->actividad_academica = $actividad_academica;
        $this->ambiente = $ambiente;
        $this->asistencia = $asistencia;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ambiente_id, $actividad_academica_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $actividad_academica = $this->actividad_academica->findOrFail($actividad_academica_id);

        $asistencias = $actividad_academica->asistenciasWithVencedores();

        return response()->json($asistencias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($ambiente_id, $actividad_academica_id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($ambiente_id, $actividad_academica_id, Request $request)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $actividad_academica = $this->actividad_academica->findOrFail($actividad_academica_id);
        $inputs = $request->input();

        $asistencia = $actividad_academica->asistencias()->create($inputs);

        $asistencias = $actividad_academica->asistenciasWithVencedores();
        return response()->json(['message' => 'Asistencia almacenada', 'list' => $asistencias, 'object' => $asistencia]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ambiente_id, $actividad_academica_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $actividad_academica = $this->actividad_academica->findOrFail($actividad_academica_id);
        $asistencia = $actividad_academica->asistencias()->where('id', $id)->first();

        return response()->json($asistencia);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ambiente_id, $actividad_academica_id, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($ambiente_id, $actividad_academica_id, Request $request, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $actividad_academica = $this->actividad_academica->findOrFail($actividad_academica_id);
        $asistencia = $actividad_academica->asistencias()->where('id', $id)->first();

        $inputs = $request->input();
        $asistencia->update($inputs);

        $asistencias = $actividad_academica->asistenciasWithVencedores();
        return response()->json(['message' => 'Asistencia almacenada', 'list' => $asistencias, 'object' => $asistencia]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ambiente_id, $actividad_academica_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $actividad_academica = $this->actividad_academica->findOrFail($actividad_academica_id);
        $asistencia = $actividad_academica->asistencias()->where('id', $id)->first();

        $asistencia->delete();

        $asistencias = $actividad_academica->asistenciasWithVencedores();
        return response()->json(['message' => 'Asistencia almacenada', 'list' => $asistencias]);
    }
}
