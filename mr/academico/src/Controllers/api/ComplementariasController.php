<?php namespace MR\Academico\Controllers\api;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\ComplementariaRequest as Request;
use MR\Academico\Models\Ambiente;
use MR\Academico\Models\ActividadAcademica;
use MR\Academico\Models\Complementaria;

class ComplementariasController extends Controller
{
    protected $ambiente;
    protected $complementaria;
    protected $actividad_academica;

    public function __construct(Complementaria $complementaria, Ambiente $ambiente, ActividadAcademica $actividad_academica)
    {
        $this->complementaria = $complementaria;
        $this->ambiente = $ambiente;
        $this->actividad_academica = $actividad_academica;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $complementarias = $ambiente->complementarias()->get();

        return response()->json($complementarias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $inputs = $request->input();

        $complementaria = $ambiente->createComplementaria($inputs);

        $complementarias = $ambiente->complementarias()->get();

        return response()->json([
            'message'=> 'actividad académica complementaria almacenada',
            'list' => $complementarias,
            'object' => $complementaria
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $complementaria = $ambiente->findComplementaria($id);

        return response()->json($complementaria);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $inputs = $request->input();

        $complementaria = $ambiente->findComplementaria($id);

        $complementaria->actividadAcademica()->update($inputs);
        $complementaria->update($inputs);

        $complementarias = $ambiente->complementarias()->get();

        return response()->json([
            'message'=> 'actividad académica complementaria almacenada',
            'list' => $complementarias,
            'object' => $complementaria
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $complementaria = $ambiente->findComplementaria($id);
        $complementaria->actividadAcademica()->delete();

        $complementarias = $ambiente->complementarias()->get();

        return response()->json([
            'message'=> 'La visualizacion de video  fue eliminada exitosamente',
            'list' => $complementarias
        ]);
    }
}
