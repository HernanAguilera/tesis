<?php namespace MR\Academico\Controllers\api;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\VisualizacionVideoclaseRequest as Request;
use MR\Academico\Models\Ambiente;
use MR\Academico\Models\ActividadAcademica;
use MR\Academico\Models\VisualizacionVideoclase;

class VisualizacionesVideoclasesController extends Controller
{

    protected $ambiente;
    protected $visualizacion_videoclase;
    protected $actividad_academica;

    public function __construct(VisualizacionVideoclase $visualizacion_videoclase, Ambiente $ambiente, ActividadAcademica $actividad_academica)
    {
        $this->visualizacion_videoclase = $visualizacion_videoclase;
        $this->ambiente = $ambiente;
        $this->actividad_academica = $actividad_academica;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $visualizaciones = $ambiente->visualizacionesVideos()->get();

        return response()->json($visualizaciones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $inputs = $request->input();

        $visualizacion = $ambiente->createVisualizacionVideoclase($inputs);

        $visualizaciones = $ambiente->visualizacionesVideos()->get();

        return response()->json([
            'message'=> 'visualizacion de video almacenada',
            'list' => $visualizaciones,
            'object' => $visualizacion
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $visualizacion = $ambiente->findVisualizacionVideoclase($id);

        return response()->json($visualizacion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $inputs = $request->input();

        $visualizacion = $ambiente->findVisualizacionVideoclase($id);

        $visualizacion->actividadAcademica()->update($inputs);
        $visualizacion->update($inputs);

        $visualizaciones = $ambiente->visualizacionesVideos()->get();

        return response()->json([
            'message'=> 'visualizacion de video almacenada',
            'list' => $visualizaciones,
            'object' => $visualizacion
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $visualizacion = $ambiente->findVisualizacionVideoclase($id);
        $visualizacion->actividadAcademica()->delete();

        $visualizaciones = $ambiente->visualizacionesVideos()->get();

        return response()->json([
            'message'=> 'La visualizacion de video  fue eliminada exitosamente',
            'list' => $visualizaciones
        ]);
    }
}
