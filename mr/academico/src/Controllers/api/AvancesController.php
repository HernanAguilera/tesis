<?php namespace MR\Academico\Controllers\api;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\AvanceRequest as Request;
use MR\Academico\Models\ProyectoSociolaboral;
use MR\Academico\Models\Ambiente;
use MR\Academico\Models\Avance;

class AvancesController extends Controller
{
    protected $ambiente;
    protected $proyecto;
    protected $avance;

    public function __construct(Ambiente $ambiente, ProyectoSociolaboral $proyecto, Avance $avance)
    {
        $this->proyecto = $proyecto;
        $this->ambiente = $ambiente;
        $this->avance = $avance;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $avances = $ambiente->avancesProyecto();
        return response()->json($avances);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);

        if ( ! $ambiente->hasProyecto() )
            return response()->json([
                'message'=> 'Este ambiente no posee proyecto socio-productivo',
                'list' => [],
                'object' => null,
            ]);

        $inputs = $request->input();
        $avance = $ambiente->proyectoSociolaboral()
                           ->first()
                           ->avances()
                           ->create($inputs)
                           ;

        $avances = $ambiente->avancesProyecto();

        return response()->json([
            'message'=> 'Avance de proyecto creado satisfactoriamente',
            'list' => $avances,
            'object' => $avance,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);

        if ( ! $ambiente->hasProyecto() )
            return response()->json([
                'message'=> 'Este ambiente no posee proyecto socio-productivo',
                'list' => [],
                'object' => null,
            ]);

        $avance = $ambiente->findAvanceProyecto($id);

        return response()->json($avance);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ambiente_id, $id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);

        if ( ! $ambiente->hasProyecto() )
            return response()->json([
                'message'=> 'Este ambiente no posee proyecto socio-productivo',
                'list' => [],
                'object' => null,
            ]);

        $inputs = $request->input();
        $avance = $ambiente->findAvanceProyecto($id);
        if ( $avance === null )
            return response()->json([
                'message'=> 'Avance no encontrado',
                'list' => [],
                'object' => null,
            ]);

        $avance->update($inputs);
        $avances = $ambiente->avancesProyecto();

        return response()->json([
            'message'=> 'Avance de proyecto actualizado satisfactoriamente',
            'list' => $avances,
            'object' => $avance,
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);

        if ( ! $ambiente->hasProyecto() )
            return response()->json([
                'message'=> 'Este ambiente no posee proyecto socio-productivo',
                'list' => [],
                'object' => null,
            ]);

        $avance = $ambiente->findAvanceProyecto($id);
        if ( $avance === null )
            return response()->json([
                'message'=> 'Avance no encontrado',
                'list' => [],
                'object' => null,
            ]);

        $avance->delete();
        $avances = $ambiente->avancesProyecto();

        return response()->json([
            'message'=> 'Avance de proyecto fue eliminado satisfactoriamente',
            'list' => $avances,
        ]);

    }
}
