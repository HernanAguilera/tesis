<?php namespace MR\Academico\Controllers\api;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\ProyectoSociolaboralRequest as Request;
use MR\Academico\Models\ProyectoSociolaboral;
use MR\Academico\Models\Ambiente;

class ProyectosSociolaboralesController extends Controller
{
    protected $ambiente;
    protected $proyecto;

    public function __construct(Ambiente $ambiente, ProyectoSociolaboral $proyecto)
    {
        $this->proyecto = $proyecto;
        $this->ambiente = $ambiente;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request, $ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);

        if ( $ambiente->hasProyecto() )
            return response()->json([
                'message'=> 'Este ambiente ya posee proyecto socio-productivo',
                'list' => [],
                'object' => null,
            ]);

        $inputs = $request->input();
        $input['ambiente_id'] = $ambiente_id;

        $proyecto = $this->proyecto->create($inputs);

        return response()->json([
            'message'=> 'proyecto socio-productivo almacenado',
            'list' => [],
            'object' => $proyecto
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getShow($ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        return response()->json($ambiente->proyectoSociolaboral()->first());
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function patchUpdate(Request $request, $ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $inputs = $request->input();

        if ( ! $ambiente->hasProyecto() )
            return response()->json([
                'message'=> 'Este ambiente no posee proyecto socio-productivo',
                'list' => [],
                'object' => null,
            ]);

        $proyecto = $ambiente->proyectoSociolaboral()->first();
        $proyecto->update($inputs);

        return response()->json([
            'message'=> 'proyecto socio-productivo actualizado',
            'list' => [],
            'object' => $proyecto
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteDestroy($ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        if ( ! $ambiente->hasProyecto() )
            return response()->json([
                'message'=> 'Este ambiente no posee proyecto socio-productivo',
                'list' => []
            ]);

        $proyecto = $ambiente->proyectoSociolaboral();
        $proyecto->delete();

        return response()->json([
            'message'=> 'El proyecto fue eliminado satisfactoriamente',
            'list' => []
        ]);
    }
}
