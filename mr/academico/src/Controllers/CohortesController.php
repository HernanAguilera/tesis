<?php namespace MR\Academico\Controllers;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\CohorteRequest as Request;
use MR\Academico\Models\Cohorte;

class CohortesController extends Controller
{
    protected $cohorte;

    public function __construct(Cohorte $cohorte)
    {
        $this->cohorte = $cohorte;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cohortes = $this->cohorte->all();

        return view('academico::cohortes.index', compact('cohortes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('academico::cohortes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        $this->cohorte->create($inputs);

        return redirect()->route('cohortes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cohorte = $this->cohorte->findOrFail($id);

        return view('academico::cohortes.show', compact('cohorte'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cohorte = $this->cohorte->findOrFail($id);

        return view('academico::cohortes.edit', compact('cohorte'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->input();
        $cohorte = $this->cohorte->findOrFail($id);
        $cohorte->update($inputs);

        return redirect()->route('cohortes.index');
    }

    public function destroy_confirm($id)
    {
        $cohorte = $this->cohorte->findOrFail($id);

        return view('academico::cohortes.destroy', compact('cohorte'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cohorte = $this->cohorte->findOrFail($id);
        $cohorte->delete();

        return redirect()->route('cohortes.index');
    }
}
