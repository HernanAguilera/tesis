<?php namespace MR\Academico\Controllers;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\FacilitadorRequest as Request;
use MR\Academico\Models\Facilitador;

class FacilitadoresController extends Controller
{
    protected $facilitador;

    public function __construct(Facilitador $facilitador)
    {
        $this->facilitador = $facilitador;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facilitadores = $this->facilitador->all();

        return view('academico::facilitadores.index', compact('facilitadores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('academico::facilitadores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        $this->facilitador->create($inputs);

        return redirect()->route('facilitadores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $facilitador = $this->facilitador->findOrFail($id);

        return view('academico::facilitadores.show', compact('facilitador'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $facilitador = $this->facilitador->findOrFail($id);

        return view('academico::facilitadores.edit', compact('facilitador'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->input();
        $facilitador = $this->facilitador->findOrFail($id);
        $facilitador->update($inputs);

        return redirect()->route('facilitadores.index');
    }


    public function destroy_confirm($id)
    {
        $facilitador = $this->facilitador->findOrFail($id);

        return view('academico::facilitadores.destroy', compact('facilitador'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $facilitador = $this->facilitador->findOrFail($id);
        $facilitador->delete();

        return redirect()->route('facilitadores.index');
    }
}
