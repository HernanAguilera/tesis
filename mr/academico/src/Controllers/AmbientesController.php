<?php namespace MR\Academico\Controllers;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\AmbienteRequest as Request;
use MR\Infraestructura\Models\Plantel;
use MR\Academico\Models\Cohorte;
use MR\Academico\Models\Semestre;
use MR\Academico\Models\Ambiente;

class AmbientesController extends Controller
{
    protected $plantel;
    protected $cohorte;
    protected $semestre;
    protected $ambiente;

    public function __construct(Plantel $plantel, Cohorte $cohorte, Semestre $semestre, Ambiente $ambiente)
    {
        $this->plantel = $plantel;
        $this->cohorte = $cohorte;
        $this->semestre = $semestre;
        $this->ambiente = $ambiente;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ambientes = $this->ambiente->all();

        return view('academico::ambientes.index', compact('ambientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vacio = ['' => '--------'];
        $planteles = $vacio + $this->plantel->lists('nombre', 'id')->toArray();
        $cohortes = $vacio + $this->cohorte->lists('descripcion', 'id')->toArray();
        $semestres = $vacio + $this->semestre->lists('descripcion', 'id')->toArray();

        return view('academico::ambientes.create', compact('planteles', 'cohortes', 'semestres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        $this->ambiente->create($inputs);

        return redirect()->route('ambientes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ambiente = $this->ambiente->findOrFail($id);

        return view('academico::ambientes.show', compact('ambiente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ambiente = $this->ambiente->findOrFail($id);

        $vacio = ['' => '--------'];
        $planteles = $vacio + $this->plantel->lists('nombre', 'id')->toArray();
        $cohortes = $vacio + $this->cohorte->lists('descripcion', 'id')->toArray();
        $semestres = $vacio + $this->semestre->lists('descripcion', 'id')->toArray();

        return view('academico::ambientes.edit', compact('ambiente', 'planteles', 'cohortes', 'semestres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->input();
        $ambiente = $this->ambiente->findOrFail($id);
        $ambiente->update($inputs);

        return redirect()->route('ambientes.index');
    }


    public function destroy_confirm($id)
    {
        $ambiente = $this->ambiente->findOrFail($id);

        return view('academico::ambientes.destroy', compact('ambiente'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ambiente = $this->ambiente->findOrFail($id);
        $ambiente->delete();

        return redirect()->route('ambientes.index');
    }
}
