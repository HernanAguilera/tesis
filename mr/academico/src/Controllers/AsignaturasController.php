<?php namespace MR\Academico\Controllers;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\AsignaturaRequest as Request;
use MR\Academico\Models\Asignatura;
use MR\Academico\Models\Semestre;

class AsignaturasController extends Controller
{

    protected $asignatura;
    protected $semestre;

    public function __construct(Asignatura $asignatura, Semestre $semestre)
    {
        $this->asignatura = $asignatura;
        $this->semestre = $semestre;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $asignaturas = $this->asignatura->all();

        return view('academico::asignaturas.index', compact('asignaturas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $semestres = $this->semestre->lists('descripcion', 'id');

        return view('academico::asignaturas.create', compact('semestres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        $this->asignatura->create($inputs);

        return redirect()->route('asignaturas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asignatura = $this->asignatura->findOrFail($id);

        return view('academico::asignaturas.show', compact('asignatura'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asignatura = $this->asignatura->findOrFail($id);
        $semestres = $this->semestre->lists('descripcion', 'id');

        return view('academico::asignaturas.edit', compact('asignatura', 'semestres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->input();
        $asignatura = $this->asignatura->findOrFail($id);
        $asignatura->update($inputs);

        return redirect()->route('asignaturas.index');
    }


    public function destroy_confirm($id)
    {
        $asignatura = $this->asignatura->findOrFail($id);

        return view('academico::asignaturas.destroy', compact('asignatura'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asignatura = $this->asignatura->findOrFail($id);
        $asignatura->delete();

        return redirect()->route('asignaturas.index');
    }
}
