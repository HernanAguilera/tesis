<?php namespace MR\Academico\Controllers;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\SemestreRequest as Request;
use MR\Academico\Models\Semestre;

class SemestresController extends Controller
{
    protected $semestre;

    public function __construct(Semestre $semestre)
    {
        $this->semestre = $semestre;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $semestres = $this->semestre->all();

        if ($request->ajax()) {
            return response()->json($semestres);
        }

        return view('academico::semestres.index', compact('semestres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('academico::semestres.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        $this->semestre->create($inputs);

        return redirect()->route('semestres.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $semestre = $this->semestre->findOrFail($id);

        return view('academico::semestres.show', compact('semestre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $semestre = $this->semestre->findOrFail($id);

        return view('academico::semestres.edit', compact('semestre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->input();
        $semestre = $this->semestre->findOrFail($id);
        $semestre->update($inputs);

        return redirect()->route('semestres.index');
    }

    public function destroy_confirm($id)
    {
        $semestre = $this->semestre->findOrFail($id);

        return view('academico::semestres.destroy', compact('semestre'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $semestre = $this->semestre->findOrFail($id);
        $semestre->delete();

        return redirect()->route('semestres.index');
    }
}
