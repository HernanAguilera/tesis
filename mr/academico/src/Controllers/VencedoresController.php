<?php namespace MR\Academico\Controllers;

use Carbon\Carbon;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\VencedorRequest as Request;
use MR\Academico\Models\Ambiente;
use MR\Academico\Models\Vencedor;

class VencedoresController extends Controller
{

    protected $vencedor;
    protected $ambiente;

    public function __construct(Ambiente $ambiente, Vencedor $vencedor)
    {
        $this->vencedor = $vencedor;
        $this->ambiente = $ambiente;

        $this->message = 'El vecedor fue incorporado al ambiente';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);

        $vencedores = $ambiente->vencedoresActivos()->get();

        return response()->json($vencedores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($ambiente_id, Request $request)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $inputs = $request->input();

        $vencedor = $ambiente->vencedores()->create($inputs);
        $ambiente->vencedores()->updateExistingPivot($vencedor->id, ['activo'=> '1', 'fecha_incorporacion' => Carbon::now()->format('Y-m-d')]);
        $vencedores = $ambiente->vencedoresActivos()->get();

        return response()->json(['message' => $this->message, 'list' => $vencedores, 'object' => $vencedor]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $vencedor = $ambiente->vencedores()->where('vencedor_id', $id)->first();

        return response()->json($vencedor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($ambiente_id ,Request $request, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $inputs = $request->input();

        $vencedor = $ambiente->vencedores()->where('vencedor_id', $id)->first();
        $vencedor->update($inputs);

        $vencedores = $ambiente->vencedoresActivos()->get();
        return response()->json(['message' => $this->message, 'list' => $vencedores, 'object' => $vencedor]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);

        //$ambiente->vencedores()->detach($id);
        $ambiente->vencedores()->updateExistingPivot($id, ['activo' => '0', 'fecha_desincorporacion' => Carbon::now()->format('Y-m-d')]);

        $vencedores = $ambiente->vencedoresActivos()->get();
        return response()->json(['message' => $this->message, 'list' => $vencedores]);
    }
}
