<?php namespace MR\Academico\Controllers;

use H34\Core\Controllers\BaseController as Controller;

use MR\Academico\Requests\AsambleaRequest as Request;
//use Illuminate\Http\Request as BaseRequest;
use MR\Academico\Models\Ambiente;
use MR\Academico\Models\ActividadAcademica;
use MR\Academico\Models\Asamblea;

class AsambleasController extends Controller
{

    protected $ambiente;
    protected $asamblea;
    protected $actividad_academica;

    public function __construct(Asamblea $asamblea, Ambiente $ambiente, ActividadAcademica $actividad_academica)
    {
        $this->asamblea = $asamblea;
        $this->ambiente = $ambiente;
        $this->actividad_academica = $actividad_academica;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $asambleas = $ambiente->asambleas()->get();

        return response()->json($asambleas);

        return view('academico::asambleas.list', compact('asambleas', 'ambiente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);

        return view('academico::asambleas.form', compact('ambiente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $ambiente_id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $inputs = $request->input();

        $asamblea = $ambiente->createAsamblea($inputs);

        $asambleas = $ambiente->asambleas()->get();

        return response()->json([
            'message'=> 'asamblea de evalucion almacenada',
            'asambleas' => $asambleas,
            'object' => $asamblea
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $asamblea = $ambiente->findAsamblea($id);
        // print_r($asamblea->actividadAcademica()->get());
        // exit();

        return response()->json($asamblea);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ambiente_id, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($ambiente_id, Request $request, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $inputs = $request->input();

        $asamblea = $ambiente->findAsamblea($id);

        $asamblea->actividadAcademica()->update($inputs);

        $asambleas = $ambiente->asambleas()->get();

        return response()->json([
            'message'=> 'asamblea de evalucion almacenada',
            'asambleas' => $asambleas,
            'object' => $asamblea
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ambiente_id, $id)
    {
        $ambiente = $this->ambiente->findOrFail($ambiente_id);
        $asamblea = $ambiente->findAsamblea($id);
        $asamblea->actividadAcademica()->delete();

        $asambleas = $ambiente->asambleas()->get();

        return response()->json([
            'message'=> 'La asamblea fue eliminada exitosamente',
            'list' => $asambleas
        ]);
    }
}
