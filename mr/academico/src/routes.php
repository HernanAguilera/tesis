<?php

use H34\Core\Utilidades\Router;

Route::group(['middleware' => ['web'], 'prefix' => 'academico'], function () {

    // Cohortes
    Router::h34resource('MR\Academico\Controllers\CohortesController', 'cohortes');

    // Dimensiones de evaluación
    Router::h34resource('MR\Academico\Controllers\DimensionesController', 'dimensiones');
    Router::h34resource('MR\Academico\Controllers\api\DimensionesController', 'api-dimensiones');

    // Semestres
    Router::h34resource('MR\Academico\Controllers\SemestresController', 'semestres');

    // Asignaturas
    Router::h34resource('MR\Academico\Controllers\AsignaturasController', 'asignaturas');
    Router::h34resource('MR\Academico\Controllers\api\AsignaturasController', 'api-asignaturas');

    // Ambientes
    Router::h34resource('MR\Academico\Controllers\AmbientesController', 'ambientes');

    // Videoclases
    Router::h34resource('MR\Academico\Controllers\VideoClasesController', 'videoclases');
    Route::group(['prefix' => 'asignaturas/{asignatura_id}'], function(){
        Router::h34resource('MR\Academico\Controllers\api\VideoClasesController', 'api-videoclases');
    });

    // Facilitadores
    Router::h34resource('MR\Academico\Controllers\FacilitadoresController', 'facilitadores');


    Route::group(['prefix' => 'ambientes/{ambiente_id}'], function(){
        // Asamblea
        Router::h34resource('MR\Academico\Controllers\AsambleasController', 'asambleas');
        Router::h34resource('MR\Academico\Controllers\VencedoresController', 'vencedores');
        Router::h34resource('MR\Academico\Controllers\api\VisualizacionesVideoclasesController', 'visualizaciones-videos');
        Router::h34resource('MR\Academico\Controllers\api\ComplementariasController', 'complementarias');
        Router::h34resource('MR\Academico\Controllers\api\AvancesController', 'avances');
        Route::group(['prefix' => 'actividad-academica/{actividad_academica_id}'], function(){
            Router::h34resource('MR\Academico\Controllers\api\EvaluacionesController', 'evaluaciones');
            Router::h34resource('MR\Academico\Controllers\api\AsistenciasController', 'asistencias');
        });
        Route::controller('proyectos-sociolaborales', 'MR\Academico\Controllers\api\ProyectosSociolaboralesController', [
            
        ]);
    });
});
