@extends('layouts.admin')

@section('work')
    <h1>Detalle de la semestre</h1>
    @include('academico::semestres.partials.detail')
    <div class="clearfix">
        <a class="button small secondary" href="{{route('semestres.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <a class="button small info" href="{{route('semestres.edit', $semestre->id)}}">
            <i class="fi-pencil"></i>
            Editar
        </a>
        <a class="button small alert right" href="{{route('semestres.destroy.confirm', $semestre->id)}}">
            <i class="fi-trash"></i>
            Eliminar
        </a>
    </div>
@stop
