@extends('layouts.admin')

@section('work')

    <h1>Nueva semestre</h1>

    {!! Form::open(['method' => 'POST', 'route' => 'semestres.store']) !!}

        {!! Form::label('descripcion', 'Descripción:') !!}
        {!! Form::text('descripcion', NULL, ['class' => 'form-control']) !!}

        <a href="{{route('semestres.index')}}" class="button small secondary">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small success">
            <i class="fi-save"></i>
            Guardar
        </button>

    {!! Form::close() !!}

@stop
