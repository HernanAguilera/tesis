@extends('layouts.admin')

@section('work')
    <h1>Semestres</h1>
    <a class="button small success" href="{{route('semestres.create')}}">
        <i class="step fi-plus size-24"></i>
        Agregar una semestre
    </a>
    @if($semestres->count())
        <table role="grid">
            <thead>
                <tr>
                    <th>
                        Descripción
                    </th>
                    <td class="table-actions">

                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($semestres as $key => $semestre)
                    <tr>
                        <td>
                            {{$semestre->descripcion}}
                        </td>
                        <td>
                            <a class="button small secondary action" href="{{route('semestres.show', $semestre->id)}}">
                                <i class="fi-zoom-in"></i>
                                Ver
                            </a>
                            <a class="button small info action" href="{{route('semestres.edit', $semestre->id)}}">
                                <i class="fi-pencil"></i>
                                Editar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert-box">
            No hay semestres registradas
        </div>
    @endif
@stop
