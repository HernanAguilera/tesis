@extends('layouts.admin')

@section('work')
    <h3 class="page-header">
        <i class="fi-alert"></i>
        Confirma que desea eliminar el registro de id: {{$semestre->id}}
        @include('academico::semestres.partials.detail')
    </h3>
    {!! Form::open(['route' => ['semestres.destroy', $semestre->id], 'method' => 'DELETE']) !!}
        <a class="button small secondary" href="{{route('semestres.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small alert">
            <i class="fi-trash"></i>
            Eliminar
        </button>
    {!! Form::close() !!}
@stop
