@extends('layouts.admin')

@section('work')

    <h1>Editar semestre</h1>

    {!! Form::model($semestre, ['method' => 'PUT', 'route' => ['semestres.update', $semestre->id]]) !!}

        {!! Form::label('descripcion', 'Descripción:') !!}
        {!! Form::text('descripcion', NULL, ['class' => 'form-control']) !!}

        <div class="clearfix">
            <a href="{{route('semestres.index')}}" class="button small secondary">
                <i class="fi-list"></i>
                Regresar
            </a>
            <button type="submit" class="button small success">
                <i class="fi-save"></i>
                Guardar
            </button>
            <a href="{{route('semestres.destroy.confirm', $semestre->id)}}" class="button small alert right ">
                <i class="fi-trash"></i>
                Eliminar
            </a>
        </div>

    {!! Form::close() !!}

@stop
