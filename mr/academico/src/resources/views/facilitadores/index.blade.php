@extends('layouts.admin')

@section('work')
    <h1>Facilitadores</h1>
    <a class="button small success" href="{{route('facilitadores.create')}}">
        <i class="step fi-plus size-24"></i>
        Agregar una dimensión
    </a>
    @if($facilitadores->count())
        <table role="grid">
            <thead>
                <tr>
                    <th>
                        Nombres
                    </th>
                    <th>
                        Apellidos
                    </th>
                    <th>
                        Cédula
                    </th>
                    <th>
                        Fecha de nacimiento
                    </th>
                    <td class="table-actions">

                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($facilitadores as $key => $facilitador)
                    <tr>
                        <td>
                            {{$facilitador->nombres}}
                        </td>
                        <td>
                            {{$facilitador->apellidos}}
                        </td>
                        <td>
                            {{$facilitador->cedula}}
                        </td>
                        <td>
                            {{$facilitador->fecha_nacimiento}}
                        </td>
                        <td>
                            <a class="button small secondary action" href="{{route('facilitadores.show', $facilitador->id)}}">
                                <i class="fi-zoom-in"></i>
                                Ver
                            </a>
                            <a class="button small info action" href="{{route('facilitadores.edit', $facilitador->id)}}">
                                <i class="fi-pencil"></i>
                                Editar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert-box">
            No hay facilitadores registradas
        </div>
    @endif
@stop
