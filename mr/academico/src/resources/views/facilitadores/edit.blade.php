@extends('layouts.admin')

@section('work')

    <h1>Editar facilitador</h1>

    {!! Form::model($facilitador, ['method' => 'PUT', 'route' => ['facilitadores.update', $facilitador->id]]) !!}

        {!! Form::label('nombres', 'Nombres:') !!}
        {!! Form::text('nombres', NULL, ['class' => 'form-control']) !!}

        {!! Form::label('apellidos', 'Apellidos:') !!}
        {!! Form::text('apellidos', NULL, ['class' => 'form-control']) !!}

        {!! Form::label('cedula', 'Cedula:') !!}
        {!! Form::text('cedula', NULL, ['class' => 'form-control']) !!}

        {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento:') !!}
        {!! Form::text('fecha_nacimiento', NULL, ['class' => 'form-control']) !!}

        <a href="{{route('facilitadores.index')}}" class="button small secondary">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small success">
            <i class="fi-save"></i>
            Guardar
        </button>
        <a href="{{route('facilitadores.destroy.confirm', $facilitador->id)}}" class="button small alert">
            <i class="fi-trash"></i>
            Eliminar
        </a>

    {!! Form::close() !!}

@stop
