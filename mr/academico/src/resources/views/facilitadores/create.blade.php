@extends('layouts.admin')

@section('work')

    <h1>Nuevo facilitador</h1>

    {!! Form::open(['method' => 'POST', 'route' => 'facilitadores.store']) !!}

        {!! Form::label('nombres', 'Nombres:') !!}
        {!! Form::text('nombres', NULL, ['class' => 'form-control']) !!}

        {!! Form::label('apellidos', 'Apellidos:') !!}
        {!! Form::text('apellidos', NULL, ['class' => 'form-control']) !!}

        {!! Form::label('cedula', 'Cedula:') !!}
        {!! Form::text('cedula', NULL, ['class' => 'form-control']) !!}

        {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento:') !!}
        {!! Form::text('fecha_nacimiento', NULL, ['class' => 'form-control date']) !!}

        <a href="{{route('facilitadores.index')}}" class="button small secondary">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small success">
            <i class="fi-save"></i>
            Guardar
        </button>

    {!! Form::close() !!}

@stop
