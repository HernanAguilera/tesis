@extends('layouts.admin')

@section('work')
    <h1>Detalle de facilitador</h1>
    @include('academico::facilitadores.partials.detail')
    <a class="button small secondary" href="{{route('facilitadores.index')}}">
        <i class="fi-list"></i>
        Regresar
    </a>
    <a class="button small info" href="{{route('facilitadores.edit', $facilitador->id)}}">
        <i class="fi-pencil"></i>
        Editar
    </a>
    <a class="button small alert" href="{{route('facilitadores.destroy.confirm', $facilitador->id)}}">
        <i class="fi-trash"></i>
        Eliminar
    </a>
@stop
