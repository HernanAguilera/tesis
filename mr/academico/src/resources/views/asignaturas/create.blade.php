@extends('layouts.admin')

@section('work')

    <h1>Nueva asignatura</h1>

    {!! Form::open(['method' => 'POST', 'route' => 'asignaturas.store']) !!}

        {!! Form::label('semestre_id', 'Semestre') !!}
        {!! Form::select('semestre_id', $semestres ,NULL, ['class' => 'form-control']) !!}

        {!! Form::label('descripcion', 'Descripción:') !!}
        {!! Form::text('descripcion', NULL, ['class' => 'form-control']) !!}

        <a href="{{route('asignaturas.index')}}" class="button small secondary">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small success">
            <i class="fi-save"></i>
            Guardar
        </button>

    {!! Form::close() !!}

@stop
