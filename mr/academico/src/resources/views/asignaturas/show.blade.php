@extends('layouts.admin')

@section('work')
    <h1>Detalle de la asignatura</h1>
    @include('academico::asignaturas.partials.detail')
    <div class="clearfix">
        <a class="button small secondary" href="{{route('asignaturas.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <a class="button small info" href="{{route('asignaturas.edit', $asignatura->id)}}">
            <i class="fi-pencil"></i>
            Editar
        </a>
        <a class="button small alert right" href="{{route('asignaturas.destroy.confirm', $asignatura->id)}}">
            <i class="fi-trash"></i>
            Eliminar
        </a>
    </div>
@stop
