@extends('layouts.admin')

@section('work')

    <h1>Editar asignatura</h1>

    {!! Form::model($asignatura, ['method' => 'PUT', 'route' => ['asignaturas.update', $asignatura->id]]) !!}

        {!! Form::label('semestre_id', 'Semestre') !!}
        {!! Form::select('semestre_id', $semestres ,NULL, ['class' => 'form-control']) !!}

        {!! Form::label('descripcion', 'Descripción:') !!}
        {!! Form::text('descripcion', NULL, ['class' => 'form-control']) !!}

        <div class="clearfix">
            <a href="{{route('asignaturas.index')}}" class="button small secondary">
                <i class="fi-list"></i>
                Regresar
            </a>
            <button type="submit" class="button small success">
                <i class="fi-save"></i>
                Guardar
            </button>
            <a href="{{route('asignaturas.destroy.confirm', $asignatura->id)}}" class="button small alert right">
                <i class="fi-trash"></i>
                Eliminar
            </a>
        </div>

    {!! Form::close() !!}

@stop
