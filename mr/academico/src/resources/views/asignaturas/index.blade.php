@extends('layouts.admin')

@section('work')
    <h1>Asignaturas</h1>
    <a class="button small success" href="{{route('asignaturas.create')}}">
        <i class="step fi-plus size-24"></i>
        Agregar una asignatura
    </a>
    @if($asignaturas->count())
        <table role="grid">
            <thead>
                <tr>
                    <th>
                        Descripción
                    </th>
                    <td class="table-actions">

                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($asignaturas as $key => $asignatura)
                    <tr>
                        <td>
                            {{$asignatura->descripcion}}
                        </td>
                        <td>
                            <a class="button small secondary action" href="{{route('asignaturas.show', $asignatura->id)}}">
                                <i class="fi-zoom-in"></i>
                                Ver
                            </a>
                            <a class="button small info action" href="{{route('asignaturas.edit', $asignatura->id)}}">
                                <i class="fi-pencil"></i>
                                Editar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert-box">
            No hay asignaturas registradas
        </div>
    @endif
@stop
