@extends('layouts.admin')

@section('work')
    <h1>Nueva asamblea de evalucación</h1>
    {!! Form::open(['method' => 'POST', 'route' => 'asambleas.store']) !!}

    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::text('descripcion', NULL, ['class' => 'form-control']) !!}

    {!! Form::label('fecha', 'Fecha en la cual se realizó:') !!}
    {!! Form::text('fecha', NULL, ['class' => 'form-control']) !!}

    <a href="{{route('asambleas.list', $ambiente->id)}}" class="button small secondary">
        <i class="fi-list"></i>
        Regresar
    </a>
    <button type="submit" class="button small success">
        <i class="fi-save"></i>
        Guardar
    </button>
    {!! Form::close() !!}
@stop
