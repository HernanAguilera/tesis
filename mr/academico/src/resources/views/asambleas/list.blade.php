@extends('layouts.admin')

@section('work')
    <h1>Asambleas de evaluación, ambiente: {{ $ambiente->nombre }}</h1>
    <a href="{{route('ambientes.show', $ambiente->id)}}" class="button small secondary">
        <i class="fi-list"></i>
        Regresar
    </a>
    <a class="button small success" href="{{route('asambleas.create', $ambiente->id)}}">
        <i class="step fi-plus size-24"></i>
        Agregar una asamblea
    </a>
    <table>
        <thead>
            <tr>
                <th>
                    Fecha
                </th>
                <th>
                    Descripción
                </th>
            </tr>
        </thead>
    </table>
@stop
