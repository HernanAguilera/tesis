@extends('layouts.admin')

@section('work')
    <h3 class="page-header">
        <i class="fi-alert"></i>
        Confirma que desea eliminar el registro de id: {{$videoclase->id}}
        @include('academico::videoclases.partials.detail')
    </h3>
    {!! Form::open(['route' => ['videoclases.destroy', $videoclase->id], 'method' => 'DELETE']) !!}
        <a class="button small secondary" href="{{route('videoclases.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small alert">
            <i class="fi-trash"></i>
            Eliminar
        </button>
    {!! Form::close() !!}
@stop
