@extends('layouts.admin')

@section('work')
    <h1>Videoclases</h1>
    <a class="button small success" href="{{route('videoclases.create')}}">
        <i class="step fi-plus size-24"></i>
        Agregar una videoclase
    </a>
    @if($videoclases->count())
        <table role="grid">
            <thead>
                <tr>
                    <th>
                        Descripción
                    </th>
                    <td class="table-actions">

                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($videoclases as $key => $videoclase)
                    <tr>
                        <td>
                            {{$videoclase->descripcion}}
                        </td>
                        <td>
                            <a class="button small secondary action" href="{{route('videoclases.show', $videoclase->id)}}">
                                <i class="fi-zoom-in"></i>
                                Ver
                            </a>
                            <a class="button small info action" href="{{route('videoclases.edit', $videoclase->id)}}">
                                <i class="fi-pencil"></i>
                                Editar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert-box">
            No hay videoclases registradas
        </div>
    @endif
@stop
