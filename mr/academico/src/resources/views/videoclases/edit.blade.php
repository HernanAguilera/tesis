@extends('layouts.admin')

@section('work')

    <h1>Editar videoclase</h1>

    {!! Form::model($videoclase, ['method' => 'PUT', 'route' => ['videoclases.update', $videoclase->id]]) !!}

        {!! Form::label('asignatura_id', 'Asignatura') !!}
        {!! Form::select('asignatura_id', $asignaturas ,NULL, ['class' => 'form-control']) !!}

        {!! Form::label('descripcion', 'Descripción:') !!}
        {!! Form::text('descripcion', NULL, ['class' => 'form-control']) !!}

        <div class="clearfix">
            <a href="{{route('videoclases.index')}}" class="button small secondary">
                <i class="fi-list"></i>
                Regresar
            </a>
            <button type="submit" class="button small success">
                <i class="fi-save"></i>
                Guardar
            </button>
            <a href="{{route('videoclases.destroy.confirm', $videoclase->id)}}" class="button small alert right">
                <i class="fi-trash"></i>
                Eliminar
            </a>
        </div>

    {!! Form::close() !!}

@stop
