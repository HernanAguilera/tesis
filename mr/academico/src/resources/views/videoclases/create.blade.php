@extends('layouts.admin')

@section('javascript')
    <script type="text/javascript">
    $.get('/semestres', function(res){
        console.log(res);
    })
    </script>
@stop

@section('work')

    <h1>Nueva videoclase</h1>

    {!! Form::open(['method' => 'POST', 'route' => 'videoclases.store']) !!}

        {!! Form::label('asignatura_id', 'Asignatura') !!}
        {!! Form::select('asignatura_id', $asignaturas ,NULL, ['class' => 'form-control']) !!}

        {!! Form::label('descripcion', 'Descripción:') !!}
        {!! Form::text('descripcion', NULL, ['class' => 'form-control']) !!}

        <a href="{{route('videoclases.index')}}" class="button small secondary">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small success">
            <i class="fi-save"></i>
            Guardar
        </button>

    {!! Form::close() !!}

@stop
