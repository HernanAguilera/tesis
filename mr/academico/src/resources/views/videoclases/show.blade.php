@extends('layouts.admin')

@section('work')
    <h1>Detalle de la videoclase</h1>
    @include('academico::videoclases.partials.detail')
    <div class="clearfix">
        <a class="button small secondary" href="{{route('videoclases.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <a class="button small info" href="{{route('videoclases.edit', $videoclase->id)}}">
            <i class="fi-pencil"></i>
            Editar
        </a>
        <a class="button small alert right" href="{{route('videoclases.destroy.confirm', $videoclase->id)}}">
            <i class="fi-trash"></i>
            Eliminar
        </a>
    </div>
@stop
