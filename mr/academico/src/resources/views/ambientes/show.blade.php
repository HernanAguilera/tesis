@extends('layouts.admin')

@section('stylesheet')
    <link rel="stylesheet" href="/packages/mr/academico/css/lib/liveSearch.css" media="screen" title="no title" charset="utf-8">
@stop

@section('javascript')
    <script src="/js/lib/jquery.mask.min.js" charset="utf-8"></script>
    <script src="/js/lib/moment-with-locales.min.js" charset="utf-8"></script>
    <script src="/js/lib/es.js" charset="utf-8"></script>
    <script src="/js/lib/angular.min.js" charset="utf-8"></script>
    <script src="/js/lib/angular-resource.min.js" charset="utf-8"></script>

    <script src="/packages/mr/academico/js/app/config.js" charset="utf-8"></script>
    <script src="/packages/mr/academico/js/lib/liveSearch.js" charset="utf-8"></script>
    <script src="/packages/mr/academico/js/app/services.js" charset="utf-8"></script>
    <script src="/packages/mr/academico/js/app/controllers/AsambleasController.js" charset="utf-8"></script>
    <script src="/packages/mr/academico/js/app/controllers/VencedoresController.js" charset="utf-8"></script>
    <script src="/packages/mr/academico/js/app/controllers/VideoclasesController.js" charset="utf-8"></script>
    <script src="/packages/mr/academico/js/app/controllers/ComplementariasController.js" charset="utf-8"></script>
    <script src="/packages/mr/academico/js/app/controllers/ProyectosController.js" charset="utf-8"></script>
    <script type="text/javascript">
        $(function () {
            $('.date').mask('00/00/0000', {placeholder:'__/__/____'});
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': H34.getCookie('XSRF-TOKEN')
                    }
            });
        });
    </script>
@stop

@section('work')
    <div id="container-app" ng-app='ambiente'>
        {{ csrf_field() }}
        <input type="hidden" id="ambiente_id" value="{{$ambiente->id}}">
        <h1>Ambiente: {{$ambiente->nombre}}</h1>
        <div class="row">
            <div class="small-12 medium-6 columns">
                <strong>Plantel:</strong> {{$ambiente->plantel->nombre}}
            </div>
            <div class="small-12 medium-6 columns">
                <strong>Cohorte:</strong> {{$ambiente->cohorte->descripcion}}
            </div>
        </div>
        <div class="row">
            <div class="small-12 medium-6 columns">
                <strong>Semestre:</strong> {{$ambiente->semestre->descripcion}}
            </div>
        </div>
        <hr>

        <!-- Tabs vencedores, asambleas, videoclases, actividades academicas alternativas -->
        <ul class="tabs" data-tab>
          <li class="tab-title active">
              <a href="#vencedores">
                  <i class="fi-torso"></i>
                  Vencedores
              </a>
          </li>
          <li class="tab-title">
              <a href="#asambleas">
                  <i class="fi-torsos-all"></i>
                  Asambleas
              </a>
          </li>
          <li class="tab-title">
              <a href="#visualizacionesvideoclases">
                  <i class="fi-play"></i>
                  Videoclases
              </a>
          </li>
          <li class="tab-title">
              <a href="#complementarias">
                  <i class="fi-puzzle"></i>
                  A. complementarias
              </a>
          </li>
          <li class="tab-title">
              <a href="#proyecto">
                  <i class="fi-book"></i>
                  Proyecto socio-laboral
              </a>
          </li>
        </ul>
        <!-- Fin de tabs -->

        <div class="panel" style="background:initial;">
            <!-- Contenido de los tabs -->
            <div class="tabs-content">
                <!-- Contenido del tab de vencedores -->
                <div class="content active" id="vencedores" ng-controller='VencedoresController'>
                    @include('academico::ambientes.partials.vencedores')
                </div>
                <!-- Fin de tab de vencedores -->

                <!-- Contenido del tab asambleas -->
                <div class="content" id="asambleas" ng-controller='AsambleasController'>
                    @include('academico::ambientes.partials.asambleas')
                </div>
                <!-- Fin del tab de asamblea -->

                <!-- Tab de visualizaciones de videos -->
                <div class="content" id="visualizacionesvideoclases" ng-controller='VideoclasesController'>
                    @include('academico::ambientes.partials.videoclases')
                </div>
                <!-- Fin de tab de visualizaciones de videos -->

                <!-- Tab de actividades academicas complementarias -->
                <div class="content" id="complementarias" ng-controller="ComplementariasController">
                    @include('academico::ambientes.partials.complementarias')
                </div>
                <!-- Fin de tab de -->

                <!-- Tab de actividades academicas complementarias -->
                <div class="content" id="proyecto" ng-controller="ProyectosController">
                    @include('academico::ambientes.partials.proyecto')
                </div>
                <!-- Fin de tab de -->
            </div>
            <!-- Fin del contenido de los tabs -->
        </div>

        <hr>
        <!-- Botones de accion sobre el elemento ambiente -->
        <div class="clearfix">
            <a class="button small secondary" href="{{route('ambientes.index')}}">
                <i class="fi-list"></i>
                Regresar
            </a>
            <a class="button small info" href="{{route('ambientes.edit', $ambiente->id)}}">
                <i class="fi-pencil"></i>
                Editar
            </a>
            <a class="button small alert right" href="{{route('ambientes.destroy.confirm', $ambiente->id)}}">
                <i class="fi-trash"></i>
                Eliminar
            </a>
        </div>
        <!-- Fin de botones de accion -->
    </div>
@stop
