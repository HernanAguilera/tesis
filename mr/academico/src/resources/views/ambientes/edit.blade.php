@extends('layouts.admin')

@section('work')

    <h1>Editar ambiente</h1>

    {!! Form::model($ambiente, ['method' => 'PUT', 'route' => ['ambientes.update', $ambiente->id]]) !!}

        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', NULL, ['class' => 'form-control']) !!}

        {!! Form::label('plantel_id', 'Plantel') !!}
        {!! Form::select('plantel_id', $planteles ,NULL, ['class' => 'form-control']) !!}

        {!! Form::label('cohorte_id', 'Cohorte') !!}
        {!! Form::select('cohorte_id', $cohortes ,NULL, ['class' => 'form-control']) !!}

        {!! Form::label('semestre_id', 'Semestre') !!}
        {!! Form::select('semestre_id', $semestres ,NULL, ['class' => 'form-control']) !!}

        <div class="clearfix">
            <a href="{{route('ambientes.index')}}" class="button small secondary">
                <i class="fi-list"></i>
                Regresar
            </a>
            <button type="submit" class="button small success">
                <i class="fi-save"></i>
                Guardar
            </button>
            <a href="{{route('ambientes.destroy.confirm', $ambiente->id)}}" class="button small alert right">
                <i class="fi-trash"></i>
                Eliminar
            </a>
        </div>

    {!! Form::close() !!}

@stop
