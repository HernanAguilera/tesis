@extends('layouts.admin')

@section('work')
    <h3 class="page-header">
        <i class="fi-alert"></i>
        Confirma que desea eliminar el registro de id: {{$ambiente->id}}
        @include('academico::ambientes.partials.detail')
    </h3>
    {!! Form::open(['route' => ['ambientes.destroy', $ambiente->id], 'method' => 'DELETE']) !!}
        <a class="button small secondary" href="{{route('ambientes.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small alert">
            <i class="fi-trash"></i>
            Eliminar
        </button>
    {!! Form::close() !!}
@stop
