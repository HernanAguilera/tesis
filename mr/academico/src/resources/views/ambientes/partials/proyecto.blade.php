<table>
    <tbody>
        <tr>
            <th>
                Titulo:
            </th>
            <td>
                @{{proyecto.titulo}}
            </td>
            <td>
                <a class="info small action" ng-click="showFormProyecto()">Editar</a>
            </td>
        </tr>
    </tbody>
</table>

<!-- Boton de agregar nueva asamblea -->
<a ng-hide='showFormularioProyecto || showFormularioAvance' class="button small success" ng-click="showFormAvance()">
  Registrar avance
</a>

<!-- Listado de avances del proyecto -->
<div ng-hide='showFormularioProyecto || showFormularioAvance'>
  <table>
      <thead>
          <tr>
              <th>
                  Descripción
              </th>
              <th>
                  Fecha de realización
              </th>
              <th class="table-actions large">

              </th>
          </tr>
      </thead>
      <tbody>
          <tr ng-repeat='avance in avances'>
              <td>
                  @{{ avance.descripcion }}
              </td>
              <td>
                  @{{ avance.fecha | date:'dd/MM/yyyy' }}
              </td>
              <td>
                  <a class="button info small action" ng-click="showFormAvance(avance.id)">Editar</a>
                  <a class='button alert small action' ng-click="showModalConfirmDeleteAvance(avance.id)">Eliminar</a>
              </td>
          </tr>
      </tbody>
  </table>
</div>

<!-- Formulario de creacion y edición de proyecto -->
<div ng-show='showFormularioProyecto'>
  <div class="clearfix">
      <a class='button success right' ng-click='saveProyecto()'>Guardar</a>
      <a class='button secondary right' ng-click='showFormularioProyecto=false'>Cancelar</a>
  </div>
  <label for="titulo_proyecto">Título:</label>
  <input type="text" id="titulo_proyecto" ng-model='proyecto.titulo'>
</div>

<!-- Formulario de creacion y edición de avance -->
<div ng-show='showFormularioAvance'>
  @{{avance|json}}
  <div class="clearfix">
      <a class='button success right' ng-click='saveAvance()'>Guardar</a>
      <a class='button secondary right' ng-click='showFormularioAvance=false'>Cancelar</a>
  </div>
  <label for="avance_descripcion">Descripción:</label>
  <input type="text" name="avance_descripcion" ng-model='avance.descripcion'>
  <label for="avance_fecha">Fecha de realización:</label>
  <input type="text" class="date" ng-model="avance.fecha">
</div>

<!-- Confrmacion de eliminacion de avance -->
<div id="confirmDeleteAvance" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <h3 id="modalTitle">¿Confirma que desea eleminar este registro?</h3>
  <p class="lead">
      <ul>
          <li>@{{ avance.descripcion }}</li>
      </ul>
      </p>
  <a class='button secondary right' ng-click="hideModalConfirmDeleteAvance()">No</a>
  <a class='button alert right' ng-click="deleteAvance(avance.id)">Si</a>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
