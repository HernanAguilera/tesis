<div class="row">
    <div class="medium-4 columns">
        <!-- Boton de agregar nuevo vencedor -->
        <a ng-hide='showFormulario' class="button small success" ng-click='crearVencedor()'>
            Agregar vencedor
        </a>
    </div>
    <!-- Area de busqueda -->
    <div class="medium-8 columns">
        <live-search id="search"
                     type="text"
                     live-search-callback="algo"
                     ng-model="vencedorSearch"></live-search>
    </div>
</div>

<!-- Listado de vencedores -->
<div ng-hide='showFormulario'>
    <table>
        <thead>
            <tr>
                <th>
                    Nombres
                </th>
                <th>
                    Apellidos
                </th>
                <th>
                    C.I.
                </th>
                <th>
                    Fecha de nac.
                </th>
                <th class="table-actions">

                </th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="vencedor in vencedores">
                <td>
                    @{{vencedor.nombres}}
                </td>
                <td>
                    @{{vencedor.apellidos}}
                </td>
                <td>
                    @{{vencedor.cedula}}
                </td>
                <td>
                    @{{vencedor.fecha_nacimiento | date:'dd/MM/yyyy'}}
                </td>
                <td>
                    <a class="button info small action" ng-click="editarVencedor(vencedor.id)">Editar</a>
                    <a class='button alert small action' ng-click="showModal(vencedor.id)">Eliminar</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<!-- Formulario de creación y edición -->
<div ng-show='showFormulario'>
    @{{vencedor|json}}
    <div class="clearfix">
        <a class='button success right' ng-click='saveVencedor()'>Guardar</a>
        <a class='button secondary right' ng-click='showFormulario=false'>Cancelar</a>
    </div>
    <label for="vencedor_nombres">Nombres:</label>
    <input type="text" id="vencedor_nombres" ng-model='vencedor.nombres'>
    <label for="vencedor_apellidos">Apellidos:</label>
    <input type="text" id="vencedor_apellidos" ng-model='vencedor.apellidos'>
    <label for="vencedor_cedula">Cédula:</label>
    <input type="text" id="vencedor_cedula" ng-model='vencedor.cedula'>
    <label for="vencedor_fecha_nacimiento">Fecha de realización:</label>
    <input type="text" id="vencedor_fecha_nacimiento" class="date" ng-model="vencedor.fecha_nacimiento">
</div>

<!-- Confirmacion de eliminacion de vencedor -->
<div id="confirmDeleteVencedor" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <h3 id="modalTitle">¿Confirma que desea desincorporar vencedor?</h3>
    <p class="lead">
        <ul>
            <li>@{{ vencedor.nombres }} @{{ vencedor.apellidos }}</li>
        </ul>
        </p>
    <a class='button secondary right' ng-click="hideModal()" >No</a>
    <a class='button alert right' ng-click="eliminarVencedor(vencedor.id)" >Si</a>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
