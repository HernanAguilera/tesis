<!-- Boton de agregar nueva asamblea -->
<a ng-hide='showFormulario || showAsistencias' class="button small success" ng-click='registrarVisualizacionvideo()'>
  Registrar nueva visualización de videoclase
</a>

<!-- Listado de visualizaciones -->
<div ng-hide='showFormulario || showAsistencias'>
  <table>
      <thead>
          <tr>
              <th>
                  Descripción
              </th>
              <th>
                  Fecha de realización
              </th>
              <th class="table-actions large">

              </th>
          </tr>
      </thead>
      <tbody>
          <tr ng-repeat='visualizacion in visualizaciones'>
              <td>
                  @{{ visualizacion.descripcion }}
              </td>
              <td>
                  @{{ visualizacion.fecha | date:'dd/MM/yyyy' }}
              </td>
              <td>
                  <a class="button success small action" ng-click="showAsignarAsistencias(visualizacion.id)">Asistencias</a>
                  <a class="button info small action" ng-click="editarVisualizacionvideo(visualizacion.id)">Editar</a>
                  <a class='button alert small action' ng-click="showModal(visualizacion.id)">Eliminar</a>
              </td>
          </tr>
      </tbody>
  </table>
</div>

<!-- Formulario de creacion y edición -->
<div ng-show='showFormulario'>
  @{{visualizacion|json}}
  <div class="clearfix">
      <a class='button success right' ng-click='saveVideoclase()'>Guardar</a>
      <a class='button secondary right' ng-click='showFormulario=false'>Cancelar</a>
  </div>
  <label for="visualizacion_descripcion">Descripción:</label>
  <input type="text" name="visualizacion_descripcion" ng-model='visualizacion.descripcion'>
  <select ng-model="visualizacion.asignatura_id" ng-change="cargarVideoclases()">
      <option value="">--------</option>
      <option ng-repeat="asignatura in asignaturas" value="@{{asignatura.id}}">@{{asignatura.descripcion}}</option>
  </select>
  <select ng-model="visualizacion.videoclase_id">
      <option value="">--------</option>
      <option ng-repeat="videoclase in videoclases" value="@{{videoclase.id}}">@{{videoclase.descripcion}}</option>
  </select>
  <label for="visualizacion_fecha">Fecha de realización:</label>
  <input type="text" class="date" ng-model="visualizacion.fecha">
</div>

<!-- Interfaz para control de asistencia -->
<div ng-show='showAsistencias'>
    <pre>
        @{{asistencias}}
    </pre>
    <table>
        <thead>
            <tr>
                <th>
                    Nombres
                </th>
                <th>
                    Apellidos
                </th>
                <th>
                    C.I.
                </th>
                <th>
                    <input type="checkbox" ng-change="asistieronTodos()" ng-model="asistencias.todos" id="asistencia-videoclases">
                    <label for="asistencia-videoclases">Asistencia</label>
                </th>
            </tr>
        </tead>
        <tbody>
            <tr ng-repeat="vencedor in asistencias">
                <td>
                    @{{vencedor.nombres}}
                </td>
                <td>
                    @{{vencedor.apellidos}}
                </td>
                <td>
                    @{{vencedor.cedula}}
                </td>
                <td>
                    <input type="checkbox" ng-model="vencedor.asistencia.asistio" ng-true-value="'1'" ng-false-value="'0'">
                </td>
            </tr>
        </tbody>
    </table>
    <a class='button small' ng-click="almacenarAsistencia()" >Guardar</a>
    <a class='button small secondary right' ng-click="cancelarAsistencias()" >No</a>
</div>
<!-- Fin de interfaz para control de asistencia -->

<!-- Confrmacion de eliminacion de asamblea -->
<div id="confirmDeleteVisualizacion" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <h3 id="modalTitle">¿Confirma que desea eleminar este registro?</h3>
  <p class="lead">
      <ul>
          <li>@{{ visualizacion.descripcion }}</li>
      </ul>
      </p>
  <a class='button secondary right' ng-click="hideModal()">No</a>
  <a class='button alert right' ng-click="eliminarVideoclase(visualizacion.id)">Si</a>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
