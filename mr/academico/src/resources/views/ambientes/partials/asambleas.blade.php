<!-- Boton de agregar nueva asamblea -->
<a ng-hide='showFormulario || showEvaluacion || showAsistencias' class="button small success" ng-click='crearAsamblea()'>
  Agregar asamblea
</a>

<!-- Listado de asambleas -->
<div ng-hide='showFormulario || showEvaluacion || showAsistencias'>
  <table>
      <thead>
          <tr>
              <th>
                  Descripción
              </th>
              <th>
                  Fecha de realización
              </th>
              <th class="table-actions xlarge">

              </th>
          </tr>
      </thead>
      <tbody>
          <tr ng-repeat='asamblea in asambleas'>
              <td>
                  @{{ asamblea.descripcion }}
              </td>
              <td>
                  @{{ asamblea.fecha | date:'dd/MM/yyyy' }}
              </td>
              <td>
                  <a class="button success small action" ng-click="showAsignarAsistencias(asamblea.id)">Asistencias</a>
                  <a class="button small action" ng-click="showEvaluar(asamblea.id)">Evaluar</a>
                  <a class="button info small action" ng-click="editarAsamblea(asamblea.id)">Editar</a>
                  <a class='button alert small action' ng-click="showModal(asamblea.id)">Eliminar</a>
              </td>
          </tr>
      </tbody>
  </table>
</div>

<!-- Formulario de creacion y edición -->
<div ng-show='showFormulario'>
  @{{asamblea|json}}
  <div class="clearfix">
      <a class='button success right' ng-click='saveAsamblea()'>Guardar</a>
      <a class='button secondary right' ng-click='showFormulario=false'>Cancelar</a>
  </div>
  <label for="asamblea_descripcion">Descripción:</label>
  <input type="text" name="asamblea_descripcion" ng-model='asamblea.descripcion'>
  <label for="asamblea_fecha">Fecha de realización:</label>
  <input type="text" class="date" ng-model="asamblea.fecha">
</div>

<!-- Interfaz para evaluacion de vencedores -->
<div ng-show='showEvaluacion'>
    <table>
        <thead>
            <tr>
                <th>
                    Nombres
                </th>
                <th>
                    Apellidos
                </th>
                <th>
                    C.I.
                </th>
                <th>
                    Valoración
                </th>
            </tr>
        </tead>
        <tbody>
            <tr ng-repeat="vencedor in vencedores">
                <td>
                    @{{vencedor.nombres}}
                </td>
                <td>
                    @{{vencedor.apellidos}}
                </td>
                <td>
                    @{{vencedor.cedula}}
                </td>
                <td>
                    <ul class="unlist">
                        <li ng-repeat="evaluacion in vencedor.evaluaciones">
                            <select ng-model="evaluacion.valoracion">
                                <option value="C">C</option>
                                <option value="A">A</option>
                                <option value="EP">P</option>
                                <option value="I">I</option>
                            </select>
                            @{{ getDimension(evaluacion.dimension_id).descripcion}}
                        </li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
    <a class='button small' ng-click="evaluar()" >Guardar</a>
    <a class='button small secondary right' ng-click="cancelarEvaluar()" >No</a>
</div>
<!-- Fin de interfaz para evaluacion de vencedores -->

<!-- Interfaz para control de asistencia -->
<div ng-show='showAsistencias'>
    <table>
        <thead>
            <tr>
                <th>
                    Nombres
                </th>
                <th>
                    Apellidos
                </th>
                <th>
                    C.I.
                </th>
                <th>
                    <input type="checkbox" ng-change="asistieronTodos()" ng-model="asistencias.todos" id="thasistencia">
                    <label for="thasistencia">Asistencia</label>
                </th>
            </tr>
        </tead>
        <tbody>
            <tr ng-repeat="vencedor in asistencias">
                <td>
                    @{{vencedor.nombres}}
                </td>
                <td>
                    @{{vencedor.apellidos}}
                </td>
                <td>
                    @{{vencedor.cedula}}
                </td>
                <td>
                    <input type="checkbox" ng-model="vencedor.asistencia.asistio" ng-true-value="'1'" ng-false-value="'0'">
                </td>
            </tr>
        </tbody>
    </table>
    <a class='button small' ng-click="almacenarAsistencia()" >Guardar</a>
    <a class='button small secondary right' ng-click="cancelarAsistencias()" >No</a>
</div>
<!-- Fin de interfaz para control de asistencia -->


<!-- Confrmacion de eliminacion de asamblea -->
<div id="confirmDeleteAsamblea" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <h3 id="modalTitle">¿Confirma que desea eleminar esta asamblea?</h3>
  <p class="lead">
      <ul>
          <li>@{{ asamblea.descripcion }}</li>
      </ul>
      </p>
  <a class='button secondary right' ng-click="hideModal()" >No</a>
  <a class='button alert right' ng-click="eliminarAsamblea(asamblea.id)" >Si</a>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
