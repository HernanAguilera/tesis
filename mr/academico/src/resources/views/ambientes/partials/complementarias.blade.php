<!-- Boton de agregar nueva asamblea -->
<a ng-hide='showFormulario || showAsistencias' class="button small success" ng-click='registrarComplementaria()'>
  Registrar nueva actividad académica complementaria
</a>

<!-- Listado de complementariaes -->
<div ng-hide='showFormulario || showAsistencias'>
  <table>
      <thead>
          <tr>
              <th>
                  Descripción
              </th>
              <th>
                  Fecha de realización
              </th>
              <th class="table-actions large">

              </th>
          </tr>
      </thead>
      <tbody>
          <tr ng-repeat='complementaria in complementarias'>
              <td>
                  @{{ complementaria.descripcion }}
              </td>
              <td>
                  @{{ complementaria.fecha | date:'dd/MM/yyyy' }}
              </td>
              <td>
              <a class="button success small action" ng-click="showAsignarAsistencias(complementaria.id)">Asistencias</a>
                  <a class="button info small action" ng-click="editarComplementaria(complementaria.id)">Editar</a>
                  <a class='button alert small action' ng-click="showModal(complementaria.id)">Eliminar</a>
              </td>
          </tr>
      </tbody>
  </table>
</div>

<!-- Formulario de creacion y edición -->
<div ng-show='showFormulario'>
  @{{complementaria|json}}
  <div class="clearfix">
      <a class='button success right' ng-click='saveComplementaria()'>Guardar</a>
      <a class='button secondary right' ng-click='showFormulario=false'>Cancelar</a>
  </div>
  <label for="complementaria_descripcion">Descripción:</label>
  <input type="text" name="complementaria_descripcion" ng-model='complementaria.descripcion'>
  <select ng-model="complementaria.tipo">
      <option value="">--------</option>
      <option value="exposicion">Exposición</option>
      <option value="debate">Debate</option>
      <option value="pruebaescrita">Prueba escrita</option>
      <option value="compartir">Compartir</option>
      <option value="otra">Otra</option>
  </select>
  <label for="complementaria_fecha">Fecha de realización:</label>
  <input type="text" class="date" ng-model="complementaria.fecha">
</div>

<!-- Interfaz para control de asistencia -->
<div ng-show='showAsistencias'>
    <pre>
        @{{asistencias}}
    </pre>
    <table>
        <thead>
            <tr>
                <th>
                    Nombres
                </th>
                <th>
                    Apellidos
                </th>
                <th>
                    C.I.
                </th>
                <th>
                    <input type="checkbox" ng-change="asistieronTodos()" ng-model="asistencias.todos" id="asistencia-complementarias">
                    <label for="asistencia-complementarias">Asistencia</label>
                </th>
            </tr>
        </tead>
        <tbody>
            <tr ng-repeat="vencedor in asistencias">
                <td>
                    @{{vencedor.nombres}}
                </td>
                <td>
                    @{{vencedor.apellidos}}
                </td>
                <td>
                    @{{vencedor.cedula}}
                </td>
                <td>
                    <input type="checkbox" ng-model="vencedor.asistencia.asistio" ng-true-value="'1'" ng-false-value="'0'">
                </td>
            </tr>
        </tbody>
    </table>
    <a class='button small' ng-click="almacenarAsistencia()" >Guardar</a>
    <a class='button small secondary right' ng-click="cancelarAsistencias()" >No</a>
</div>
<!-- Fin de interfaz para control de asistencia -->

<!-- Confrmacion de eliminacion de asamblea -->
<div id="confirmDeleteComplementaria" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <h3 id="modalTitle">¿Confirma que desea eleminar este registro?</h3>
  <p class="lead">
      <ul>
          <li>@{{ complementaria.descripcion }}</li>
      </ul>
      </p>
  <a class='button secondary right' ng-click="hideModal()">No</a>
  <a class='button alert right' ng-click="eliminarComplementaria(complementaria.id)">Si</a>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
