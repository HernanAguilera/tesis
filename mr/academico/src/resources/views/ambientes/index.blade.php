@extends('layouts.admin')

@section('work')
    <h1>Ambientes</h1>
    <a class="button small success" href="{{route('ambientes.create')}}">
        <i class="step fi-plus size-24"></i>
        Agregar una ambiente
    </a>
    @if($ambientes->count())
        <table role="grid">
            <thead>
                <tr>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Plantel
                    </th>
                    <th>
                        Cohorte
                    </th>
                    <th>
                        Semestre
                    </th>
                    <td class="table-actions">

                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($ambientes as $key => $ambiente)
                    <tr>
                        <td>
                            {{$ambiente->nombre}}
                        </td>
                        <td>
                            {{$ambiente->plantel->nombre}}
                        </td>
                        <td>
                            {{$ambiente->cohorte->descripcion}}
                        </td>
                        <td>
                            {{$ambiente->semestre->descripcion}}
                        </td>
                        <td>
                            <a class="button small secondary action" href="{{route('ambientes.show', $ambiente->id)}}">
                                <i class="fi-zoom-in"></i>
                                Ver
                            </a>
                            <a class="button small info action" href="{{route('ambientes.edit', $ambiente->id)}}">
                                <i class="fi-pencil"></i>
                                Editar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert-box">
            No hay ambientes registrados
        </div>
    @endif
@stop
