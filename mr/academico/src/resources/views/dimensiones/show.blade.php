@extends('layouts.admin')

@section('work')
    <h1>Detalle de la dimensión</h1>
    @include('academico::dimensiones.partials.detail')
    <div class="clearfix">
        <a class="button small secondary" href="{{route('dimensiones.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <a class="button small info" href="{{route('dimensiones.edit', $dimension->id)}}">
            <i class="fi-pencil"></i>
            Editar
        </a>
        <a class="button small alert right" href="{{route('dimensiones.destroy.confirm', $dimension->id)}}">
            <i class="fi-trash"></i>
            Eliminar
        </a>
    </div>
@stop
