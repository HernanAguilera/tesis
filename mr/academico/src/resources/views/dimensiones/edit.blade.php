@extends('layouts.admin')

@section('work')

    <h1>Editar dimensión</h1>

    {!! Form::model($dimension, ['method' => 'PUT', 'route' => ['dimensiones.update', $dimension->id]]) !!}

        {!! Form::label('descripcion', 'Descripción:') !!}
        {!! Form::text('descripcion', NULL, ['class' => 'form-control']) !!}

        <div class="clearfix">
            <a href="{{route('dimensiones.index')}}" class="button small secondary">
                <i class="fi-list"></i>
                Regresar
            </a>
            <button type="submit" class="button small success">
                <i class="fi-save"></i>
                Guardar
            </button>
            <a href="{{route('dimensiones.destroy.confirm', $dimension->id)}}" class="button small alert right">
                <i class="fi-trash"></i>
                Eliminar
            </a>
        </div>

    {!! Form::close() !!}

@stop
