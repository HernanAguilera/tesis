@extends('layouts.admin')

@section('work')
    <h1>Dimensiones de evaluación</h1>
    <a class="button small success" href="{{route('dimensiones.create')}}">
        <i class="step fi-plus size-24"></i>
        Agregar una dimensión
    </a>
    @if($dimensiones->count())
        <table role="grid">
            <thead>
                <tr>
                    <th>
                        Descripción
                    </th>
                    <td class="table-actions">

                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($dimensiones as $key => $dimension)
                    <tr>
                        <td>
                            {{$dimension->descripcion}}
                        </td>
                        <td>
                            <a class="button small secondary action" href="{{route('dimensiones.show', $dimension->id)}}">
                                <i class="fi-zoom-in"></i>
                                Ver
                            </a>
                            <a class="button small info action" href="{{route('dimensiones.edit', $dimension->id)}}">
                                <i class="fi-pencil"></i>
                                Editar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert-box">
            No hay dimensiones registradas
        </div>
    @endif
@stop
