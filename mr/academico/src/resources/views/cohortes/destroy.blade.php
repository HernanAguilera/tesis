@extends('layouts.admin')

@section('work')
    <h3 class="page-header">
        <i class="fi-alert"></i>
        Confirma que desea eliminar el registro de id: {{$cohorte->id}}
        @include('academico::cohortes.partials.detail')
    </h3>
    {!! Form::open(['route' => ['cohortes.destroy', $cohorte->id], 'method' => 'DELETE']) !!}
        <a class="button small secondary" href="{{route('cohortes.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small alert">
            <i class="fi-trash"></i>
            Eliminar
        </button>
    {!! Form::close() !!}
@stop
