@extends('layouts.admin')

@section('work')

    <h1>Editar cohorte</h1>

    {!! Form::model($cohorte, ['method' => 'PUT', 'route' => ['cohortes.update', $cohorte->id]]) !!}

        {!! Form::label('descripcion', 'Descripción:') !!}
        {!! Form::text('descripcion', NULL, ['class' => 'form-control']) !!}

        <div class="clearfix">
            <a href="{{route('cohortes.index')}}" class="button small secondary">
                <i class="fi-list"></i>
                Regresar
            </a>
            <button type="submit" class="button small success">
                <i class="fi-save"></i>
                Guardar
            </button>
            <a href="{{route('cohortes.destroy.confirm', $cohorte->id)}}" class="button small alert right">
                <i class="fi-trash"></i>
                Eliminar
            </a>
        </div>

    {!! Form::close() !!}

@stop
