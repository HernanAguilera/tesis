@extends('layouts.admin')

@section('work')
    <h1>Cohortes</h1>
    <a class="button small success" href="{{route('cohortes.create')}}">
        <i class="step fi-plus size-24"></i>
        Agregar una cohorte
    </a>
    @if($cohortes->count())
        <table role="grid">
            <thead>
                <tr>
                    <th>
                        Descripción
                    </th>
                    <td class="table-actions">

                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($cohortes as $key => $cohorte)
                    <tr>
                        <td>
                            {{$cohorte->descripcion}}
                        </td>
                        <td>
                            <a class="button small secondary action" href="{{route('cohortes.show', $cohorte->id)}}">
                                <i class="fi-zoom-in"></i>
                                Ver
                            </a>
                            <a class="button small info action" href="{{route('cohortes.edit', $cohorte->id)}}">
                                <i class="fi-pencil"></i>
                                Editar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert-box">
            No hay cohortes registradas
        </div>
    @endif
@stop
