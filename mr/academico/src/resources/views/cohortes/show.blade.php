@extends('layouts.admin')

@section('work')
    <h1>Detalle de la cohorte</h1>
    @include('academico::cohortes.partials.detail')
    <div class="clearfix">
        <a class="button small secondary" href="{{route('cohortes.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <a class="button small info" href="{{route('cohortes.edit', $cohorte->id)}}">
            <i class="fi-pencil"></i>
            Editar
        </a>
        <a class="button small alert right" href="{{route('cohortes.destroy.confirm', $cohorte->id)}}">
            <i class="fi-trash"></i>
            Eliminar
        </a>
    </div>
@stop
