<?php namespace MR\Academico;

use Illuminate\Support\ServiceProvider;

class AcademicoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';

        $this->loadViewsFrom(__DIR__.'/resources/views', 'academico');

        $this->publishes([
            __DIR__.'/assets' => public_path('packages/mr/academico'),
        ], 'public');

        $this->publishes([
			__DIR__ . '/database/migrations/' => base_path('/database/migrations'),
		], 'migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
