## Sistema para la gestión de data asociada para las actidades academicas

Software desarrollado con [Laravel](http://laravel.com) 5.1 

### Preconfiguración

En config/app.php agregar los services providers:

```
H34\Core\CoreServiceProvider::class,
H34\Regionalizacion\RegionalizacionServiceProvider::class,
H34\Autenticacion\AutenticacionServiceProvider::class,
H34\MensajeriaInterna\MensajeriaInternaServiceProvider::class,
```

### Instalación

 ```
 ~$ php artisan vendor:publish
 ~$ php artisan migrate:install
 ~$ php artisan migrate
 ~$ php artisan admin:install
 ```

### Crenciales de ingreso

```
User: admin
Password: SleepingOwl
```


### Configuración

En config/auth.php cambiar las claves:

'model' => H34\Autenticacion\Models\Usuario::class,
'table' => 'usuarios',

### License

Este proyecto es open source bajo la licencia [MIT license](http://opensource.org/licenses/MIT)
