@extends('layouts.admin')

@section('work')

    <h1>Nueva permiso</h1>

    {!! Form::open(['method' => 'POST', 'route' => 'permisos.store']) !!}

        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', NULL, ['class' => 'form-control']) !!}

        {!! Form::label('roles', 'Roles:') !!}
        {!! Form::select('roles[]', $roles, NULL, ['multiple' => 'multiple', 'class' => 'chosen-select', 'data-placeholder' => 'Seleccione una opción']) !!}

        <a href="{{route('permisos.index')}}" class="button small secondary">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small success">
            <i class="fi-save"></i>
            Guardar
        </button>

    {!! Form::close() !!}

@stop
