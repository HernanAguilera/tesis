@extends('layouts.admin')

@section('work')
    <h1>Detalle de permiso</h1>
    @include('firewall::permisos.partials.detail')
    <div class="clearfix">
        <a class="button small secondary" href="{{route('permisos.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <a class="button small info" href="{{route('permisos.edit', $permiso->id)}}">
            <i class="fi-pencil"></i>
            Editar
        </a>
        <a class="button small alert right" href="{{route('permisos.destroy.confirm', $permiso->id)}}">
            <i class="fi-trash"></i>
            Eliminar
        </a>
    </div>
@stop
