@extends('layouts.admin')

@section('work')

    <h1>Editar usuario</h1>

    {!! Form::model($usuario, ['method' => 'PUT', 'route' => ['usuarios.update', $usuario->id]]) !!}

        {!! Form::label('username', 'Nombre de usuario:') !!}
        {!! Form::text('username', NULL, []) !!}

        {!! Form::label('email', 'Correo eletrónico:') !!}
        {!! Form::text('email', NULL, []) !!}

        {!! Form::label('password', 'Contraseña:') !!}
        {!! Form::password('password', NULL, []) !!}

        {!! Form::label('password-repeat', 'Repetir contraseña:') !!}
        {!! Form::password('password-repeat', NULL, []) !!}

        {!! Form::label('rol', 'Rol:') !!}
        {!! Form::select('rol', $roles, $usuario_rol_id, []) !!}

        <div class="clearfix">
            <a href="{{route('usuarios.index')}}" class="button small secondary">
                <i class="fi-list"></i>
                Regresar
            </a>
            <button type="submit" class="button small success">
                <i class="fi-save"></i>
                Guardar
            </button>
            <a href="{{route('usuarios.destroy.confirm', $usuario->id)}}" class="button small alert right ">
                <i class="fi-trash"></i>
                Eliminar
            </a>
        </div>

    {!! Form::close() !!}

@stop
