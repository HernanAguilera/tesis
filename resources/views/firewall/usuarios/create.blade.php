@extends('layouts.admin')

@section('work')

    <h1>Nueva usuario</h1>

    {!! Form::open(['method' => 'POST', 'route' => 'usuarios.store']) !!}

        {!! Form::label('username', 'Nombre de usuario:') !!}
        {!! Form::text('username', NULL, []) !!}

        {!! Form::label('email', 'Correo eletrónico:') !!}
        {!! Form::text('email', NULL, []) !!}

        {!! Form::label('password', 'Contraseña:') !!}
        {!! Form::password('password', NULL, []) !!}

        {!! Form::label('password-repeat', 'Repetir contraseña:') !!}
        {!! Form::password('password-repeat', NULL, []) !!}

        {!! Form::label('rol', 'Rol:') !!}
        {!! Form::select('rol', $roles, NULL, []) !!}

        <a href="{{route('usuarios.index')}}" class="button small secondary">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small success">
            <i class="fi-save"></i>
            Guardar
        </button>

    {!! Form::close() !!}

@stop
