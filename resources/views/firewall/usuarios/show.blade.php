@extends('layouts.admin')

@section('work')
    <h1>Detalle de la usuario</h1>
    @include('firewall::usuarios.partials.detail')
    <div class="clearfix">
        <a class="button small secondary" href="{{route('usuarios.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <a class="button small info" href="{{route('usuarios.edit', $usuario->id)}}">
            <i class="fi-pencil"></i>
            Editar
        </a>
        <a class="button small alert right" href="{{route('usuarios.destroy.confirm', $usuario->id)}}">
            <i class="fi-trash"></i>
            Eliminar
        </a>
    </div>
@stop
