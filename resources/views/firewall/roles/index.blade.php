@extends('layouts.admin')

@section('work')
    <h1>Roles de usuarios</h1>
    <a class="button small success" href="{{route('roles.create')}}">
        <i class="step fi-plus size-24"></i>
        Agregar un nuevo rol
    </a>
    @if($roles->count())
        <table role="grid">
            <thead>
                <tr>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Descripción
                    </th>
                    <td class="table-actions">

                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($roles as $key => $rol)
                    <tr>
                        <td>
                            {{$rol->nombre}}
                        </td>
                        <td>
                            {{$rol->description}}
                        </td>
                        <td>
                            <a class="button small secondary action" href="{{route('roles.show', $rol->id)}}">
                                <i class="fi-zoom-in"></i>
                                Ver
                            </a>
                            <a class="button small info action" href="{{route('roles.edit', $rol->id)}}">
                                <i class="fi-pencil"></i>
                                Editar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert-box">
            No hay roles registradas
        </div>
    @endif
@stop
