@extends('layouts.admin')

@section('work')

    <h1>Editar rol de usuario</h1>

    {!! Form::model($rol, ['method' => 'PUT', 'route' => ['roles.update', $rol->id]]) !!}

        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', NULL, []) !!}

        {!! Form::label('permisos', 'Permisos:') !!}
        {!! Form::select('permisos[]', $permisos, $permisos_rol, ['multiple' => 'multiple', 'class' => 'chosen-select', 'data-placeholder' => 'Seleccione una opción']) !!}

        {!! Form::label('description', 'Descripción:') !!}
        {!! Form::textArea('description', NULL, []) !!}

        <div class="clearfix">
            <a href="{{route('roles.index')}}" class="button small secondary">
                <i class="fi-list"></i>
                Regresar
            </a>
            <button type="submit" class="button small success">
                <i class="fi-save"></i>
                Guardar
            </button>
            <a href="{{route('roles.destroy.confirm', $rol->id)}}" class="button small alert right">
                <i class="fi-trash"></i>
                Eliminar
            </a>
        </div>

    {!! Form::close() !!}

@stop
