@extends('layouts.admin')

@section('work')

    <h1>Nueva rol</h1>

    {!! Form::open(['method' => 'POST', 'route' => 'roles.store']) !!}

        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', NULL, []) !!}

        {!! Form::label('permisos', 'Permisos:') !!}
        {!! Form::select('permisos[]', $permisos, NULL, ['multiple' => 'multiple', 'class' => 'chosen-select', 'data-placeholder' => 'Seleccione una opción']) !!}

        {!! Form::label('description', 'Descripción:') !!}
        {!! Form::textArea('description', NULL, []) !!}

        <a href="{{route('roles.index')}}" class="button small secondary">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small success">
            <i class="fi-save"></i>
            Guardar
        </button>

    {!! Form::close() !!}

@stop
