@extends('layouts.body')

@section('content')

    <div class="row">
        <h1>Página principal</h1>
        <div class="medium-4 columns">
            <div class="panel callout radius">
              <h3>Infraestructura.</h3>
              <ul>
                  <li><a href="{{route('planteles.index')}}">Planteles</a></li>
              </ul>
            </div>
        </div>
        <div class="medium-4 columns">
            <div class="panel callout radius">
              <h3>Academico.</h3>
              <ul>
                  <li><a href="{{route('cohortes.index')}}">Cohortes</a></li>
              </ul>
            </div>
        </div>
        <div class="medium-4 columns">
            <div class="panel callout radius">
              <h3>Gestión de usuarios.</h3>
              <ul>
                  <li><a href="{{route('usuarios.index')}}">Usuarios</a></li>
                  <li><a href="{{route('roles.index')}}">Roles</a></li>
                  <li><a href="{{route('permisos.index')}}">Permisos</a></li>
              </ul>
            </div>
        </div>
    </div>
@stop
