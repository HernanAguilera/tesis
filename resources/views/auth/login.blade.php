@extends('layouts.body')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="medium-6 medium-offset-3">
            @include('layouts.fragments.messages')
			<div class="panel">
                <h4 style="text-align:center;">Inicio de sesión</h4>
                <hr>
                <p style="text-align:center;">
                    <img src="/images/misionribas_250x197b.jpg" alt="logo misión ribas" />
                </p>
                <hr>
				<div>
                    @if (count($errors) > 0)
						<div class="alert-box alert">
                            <p style="color:white;">
                                Hay errores en los campos de entrad.
                            </p>
							<ul>
								@foreach ($errors->all() as $error)
									<li style="color:white;">{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<form class="form-horizontal" role="form" method="POST" action="/auth/login">
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">

						<div class="row">
                            <div class="large-4 medium-4 small-4 columns">
                                <label class="inline right" for="email">Correo electrónico</label>
                            </div>
							<div class="large-8 medium-8 small-8 columns">
								<input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="row">
                            <div class="large-4 medium-4 small-4 columns">
                                <label class="inline right" for="password">Contraseña</label>
                            </div>
							<div class="large-8 medium-8 small-8 columns">
								<input id="password" type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="row">
							<div style="text-align:center;">
								<button type="submit" class="btn btn-primary" style="margin-right: 15px;">
									Iniciar sesión
								</button>

								<!--<a href="/password/email">¿Olvidaste tu contraseña?</a>-->
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
