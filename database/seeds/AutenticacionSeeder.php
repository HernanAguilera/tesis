<?php

use Illuminate\Database\Seeder;

class AutenticacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'nombre' => 'administrador de la aplicación',
                'description' => 'Usuario administrador del sistema'
            ],
            [
                'nombre' => 'Coordinador de programas educativos',
                'description' => 'Usuario encargado de administrar el área académica'
            ],
            [
                'nombre' => 'coordinador municipal',
                'description' => 'Usuario con posiblidad de generar reportes en la aplicación'
            ]
        ];

        $permisos = [
            [ 'nombre' => 'es_administrador'],
            [ 'nombre' => 'listar_planteles'],
            [ 'nombre' => 'listar_ambientes'],
            [ 'nombre' => 'listar_dimensiones'],
            [ 'nombre' => 'listar_cohortes'],
        ];

        $permiso_rol = [
            [
                'permiso_id' => 1,
                'rol_id' => 1
            ]
        ];

        $usuarios = [
        	[
                'email' => 'admin@ejemplo.org',
        		'password' => \Hash::make('123456'),
                'verificado' => TRUE,
        	]
        ];

        $rol_usuario = array(
            array(
                'rol_id' => 1,
                'usuario_id' => 1,
            )
        );

        \DB::table('roles')->insert($roles);
        \DB::table('permisos')->insert($permisos);
        \DB::table('permiso_rol')->insert($permiso_rol);
        \DB::table('usuarios')->insert($usuarios);
        \DB::table('rol_usuario')->insert($rol_usuario);
    }
}
