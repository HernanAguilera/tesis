<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectosSociolaboralesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos_sociolaborales', function (Blueprint $table) {
            $table->integer('ambiente_id')->unsigned()->index();
            $table->string('titulo', 256)->nullable();
            $table->timestamps();
            $table->foreign('ambiente_id')->references('id')->on('ambientes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proyectos_sociolaborales');
    }
}
