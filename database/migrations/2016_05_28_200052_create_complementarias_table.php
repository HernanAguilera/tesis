<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplementariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complementarias', function (Blueprint $table) {
            $table->integer('actividad_academica_id')->unsigned()->index();
            $table->string('tipo', 64);
            $table->foreign('actividad_academica_id')->references('id')->on('actividades_academicas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('complementarias');
    }
}
