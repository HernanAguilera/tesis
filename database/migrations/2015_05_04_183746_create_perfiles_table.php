<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('perfiles', function(Blueprint $table)
		{
			$table->integer('usuario_id')->unsigned()->index();
			$table->string('nombres', 128)->nullable();
			$table->string('apellidos', 128)->nullable();
			$table->integer('ciudad_id')->unsigned()->index()->nullable();
			$table->string('web', 128)->nullable();
			$table->string('avatar')->nullable();

			$table->foreign('usuario_id')
				  ->references('id')
				  ->on('usuarios')
				  ;

			$table->foreign('ciudad_id')
				  ->references('id')
				  ->on('ciudades')
				  ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('perfiles');
	}

}
