<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbienteFacilitadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambiente_facilitador', function (Blueprint $table) {
            $table->integer('ambiente_id')->unsigned()->index();
			$table->foreign('ambiente_id')->references('id')->on('ambientes')->onDelete('cascade');
			$table->integer('facilitador_id')->unsigned()->index();
			$table->foreign('facilitador_id')->references('id')->on('facilitadores')->onDelete('cascade');
            $table->boolean('activo');
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ambiente_facilitador');
    }
}
