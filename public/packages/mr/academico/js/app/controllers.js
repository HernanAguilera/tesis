angular.module('ambiente')
.controller('AsambleasController', function ($scope, AsambleaResource, CSRF_TOKEN) {
    resourceAsamblea = AsambleaResource.create($('#ambiente_id').val());

    $scope.showFormulario = false;
    $scope.asambleas = resourceAsamblea.query();

    $scope.crearAsamblea = function () {
        $scope.asamblea = {};
        $scope.asamblea.fecha = moment().format('DD/MM/YYYY');
        $scope.showFormulario = true;
    }

    $scope.saveAsamblea = function () {
        data = $scope.asamblea
        data['fecha'] = moment($scope.asamblea.fecha, 'DD/MM/YYYY').format('YYYY-MM-DD');
        data['_token'] = CSRF_TOKEN;
        console.log('enviada', data);

        if( typeof $scope.asamblea.id !== 'undefined'){
            resourceAsamblea.update({id:$scope.asamblea.id}, data, function (data) {
                console.log(data)
                $scope.asambleas = data.asambleas;
                $scope.showFormulario = false;
            }, function (error) {
                console.log(error);
            });
        }else {
            resourceAsamblea.save(data, function (data) {
                console.log(data);
                $scope.asambleas = data.asambleas;
                $scope.showFormulario = false;
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.editarAsamblea = function (id) {
        $scope.asamblea = resourceAsamblea.get({id:id}, function () {
            $scope.asamblea.fecha = moment($scope.asamblea.fecha, 'YYYY-MM-DD').format('DD/MM/YYYY');
            $scope.showFormulario = true;
        });
    }

    $scope.showModal = function (id) {
        $scope.asamblea = resourceAsamblea.get({id:id});
        $('#confirmDeleteAsamblea').foundation('reveal', 'open');
    }

    $scope.hideModal = function () {
        $('#confirmDeleteAsamblea').foundation('reveal', 'close');
    }

    $scope.eliminarAsamblea = function (id) {
        resourceAsamblea.delete({id: id}, function(data){
            console.log(data);
            $scope.asambleas = data.asambleas;
        });
        $scope.hideModal();
        console.log('Eliminar id: ', id);
    }
})
.controller('VencedoresController', function ($scope, VencedorResource, CSRF_TOKEN) {
    resourceVencedor = VencedorResource.create($('#ambiente_id').val());

    $scope.showFormulario = false;
    $scope.vencedores = resourceVencedor.query();

    $scope.crearVencedor = function () {
        $scope.vencedor = {};
        $scope.showFormulario = true;
    }

    $scope.saveVencedor = function () {
        data = $scope.vencedor
        data['fecha_nacimiento'] = moment($scope.vencedor.fecha_nacimiento, 'DD/MM/YYYY').format('YYYY-MM-DD');
        data['_token'] = CSRF_TOKEN;
        console.log('enviada', data);

        if( typeof $scope.vencedor.id !== 'undefined'){
            resourceVencedor.update({id:$scope.vencedor.id}, data, function (data) {
                console.log(data);
                $scope.vencedores = data.list;
                $scope.showFormulario = false;
            }, function (error) {
                console.log(error);
            });
        }else {
            resourceVencedor.save(data, function (data) {
                console.log(data);
                $scope.vencedores = data.list;
                $scope.showFormulario = false;
            }, function (error) {
                console.log(error);
            });
        }

        $scope.algo = function () {

        }
    }

    $scope.editarVencedor = function (id) {
        $scope.vencedor = resourceVencedor.get({id:id}, function () {
            $scope.vencedor.fecha_nacimiento = moment($scope.vencedor.fecha_nacimiento, 'YYYY-MM-DD').format('DD/MM/YYYY');
            $scope.showFormulario = true;
        });
    }

    $scope.showModal = function (id) {
        $scope.vencedor = resourceVencedor.get({id:id});
        $('#confirmDeleteVencedor').foundation('reveal', 'open');
    }

    $scope.hideModal = function () {
        $('#confirmDeleteVencedor').foundation('reveal', 'close');
    }

    $scope.eliminarVencedor = function (id) {
        resourceVencedor.delete({id: id}, function(data){
            console.log(data);
            $scope.vencedores = data.list;
        });
        $scope.hideModal();
        console.log('Eliminar id: ', id);
    }

})
;
