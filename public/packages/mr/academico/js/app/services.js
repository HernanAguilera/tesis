angular.module('ambiente')
.service('HttpLocal', function ($http) {
    this.host = window.location.protocol + '//' + window.location.host;
    this.get = function (path, data) {
        return $http.get(this.host + path, data);
    }
    this.post = function (path, data) {
        return $http.post(this.host + path, data);
    }
})
.service("AsambleaResource", function ($resource, HOST) {
    this.create = function (ambiente_id) {
        ambiente_id = ambiente_id || null;
        if(ambiente_id === null)
            return;
        return $resource(HOST + '/academico/ambientes/:ambiente_id/asambleas/:id/',
                        {ambiente_id: ambiente_id, id: '@id'},
                        { update:{method:'patch'} },
                        { stripTrailingSlashes: false });
    }
})
.service("VencedorResource", function ($resource, HOST) {
    this.create = function (ambiente_id) {
        ambiente_id = ambiente_id || null;
        if(ambiente_id === null)
            return;
        return $resource(HOST + '/academico/ambientes/:ambiente_id/vencedores/:id/',
                        {ambiente_id: ambiente_id, id: '@id'},
                        { update:{method:'patch'} },
                        { stripTrailingSlashes: false });
    }
})
.service("VisualizacionVideoclaseResource", function ($resource, HOST) {
    this.create = function (ambiente_id) {
        ambiente_id = ambiente_id || null;
        if(ambiente_id === null)
            return;
        return $resource(HOST + '/academico/ambientes/:ambiente_id/visualizaciones-videos/:id/',
                        {ambiente_id: ambiente_id, id: '@id'},
                        { update:{method:'patch'} },
                        { stripTrailingSlashes: false }
                    );
    }
})
.service("ComplementariasResource", function ($resource, HOST) {
    this.create = function (ambiente_id) {
        ambiente_id = ambiente_id || null;
        if(ambiente_id === null)
            return;
        return $resource(HOST + '/academico/ambientes/:ambiente_id/complementarias/:id/',
                        {ambiente_id: ambiente_id, id: '@id'},
                        { update:{method:'patch'} },
                        { stripTrailingSlashes: false }
                    );
    }
})
.service("AvanceResource", function ($resource, HOST) {
    this.create = function (ambiente_id) {
        ambiente_id = ambiente_id || null;
        if(ambiente_id === null)
            return;
        return $resource(HOST + '/academico/ambientes/:ambiente_id/avances/:id/',
                        {ambiente_id: ambiente_id, id: '@id'},
                        { update:{method:'patch'} },
                        { stripTrailingSlashes: false }
                    );
    }
})
.service("EvaluacionesResource", function ($resource, HOST) {
    this.create = function (ambiente_id, actividad_academica_id) {
        ambiente_id = ambiente_id || null;
        actividad_academica_id = actividad_academica_id || null;
        if (ambiente_id === null || actividad_academica_id === null)
            return;
        return $resource(HOST + '/academico/ambientes/:ambiente_id/actividad-academica/:actividad_academica_id/evaluaciones/:id/',
                            {ambiente_id: ambiente_id, actividad_academica_id: actividad_academica_id, id: '@id'},
                            { update:{method:'patch'} },
                            { stripTrailingSlashes: false }
                        )
    }
})
.service("AsistenciasResource", function ($resource, HOST) {
    this.create = function (ambiente_id, actividad_academica_id) {
        ambiente_id = ambiente_id || null;
        actividad_academica_id = actividad_academica_id || null;
        if (ambiente_id === null || actividad_academica_id === null)
            return;
        return $resource(HOST + '/academico/ambientes/:ambiente_id/actividad-academica/:actividad_academica_id/asistencias/:id/',
                            {ambiente_id: ambiente_id, actividad_academica_id: actividad_academica_id, id: '@id'},
                            { update:{method:'patch'} },
                            { stripTrailingSlashes: false }
                        )
    }
})
.factory("DimensionesResource", function ($resource, HOST) {
    return $resource(HOST + '/academico/api-dimensiones/:id/',
                    {id: '@id'},
                    { update:{method:'patch'} },
                    { stripTrailingSlashes: false });
})
.factory("AsignaturasResource", function ($resource, HOST) {
    return $resource(HOST + '/academico/api-asignaturas/:id/',
                    {id: '@id'},
                    { update:{method:'patch'} },
                    { stripTrailingSlashes: false });
})
.service("VideoClasesResource", function ($resource, HOST) {
    this.create = function (asignatura_id) {
        asignatura_id = asignatura_id || null;
        if(asignatura_id === null)
            return;
        return $resource(HOST + '/academico/asignaturas/:asignatura_id/api-videoclases/:id/',
                        {asignatura_id: asignatura_id, id: '@id'},
                        { update:{method:'patch'} },
                        { stripTrailingSlashes: false }
                    );
    }
})
;
