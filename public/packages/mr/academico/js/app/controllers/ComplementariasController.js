angular.module('ambiente')
.controller('ComplementariasController', function ($scope, ComplementariasResource, AsistenciasResource) {
    resourceComplementaria = ComplementariasResource.create($('#ambiente_id').val());

    $scope.showFormulario = false; // En inicio el formulario creación/edición permanece oculto
    $scope.showAsistencias = false; // En inicio el formulario de asistencia permanece oculto

    $scope.complementarias = resourceComplementaria.query(); // Se obtiene la lista de videoclases vistas

    // Inicializa el objeto asamblea y activa la bandera de mostar formulario
    $scope.registrarComplementaria = function () {
        $scope.complementaria = {};
        $scope.complementaria.fecha = moment().format('DD/MM/YYYY');
        $scope.showFormulario = true;
    }

    $scope.saveComplementaria = function () {
        data = $scope.complementaria
        data['fecha'] = moment($scope.complementaria.fecha, 'DD/MM/YYYY').format('YYYY-MM-DD');

        if( typeof $scope.complementaria.id !== 'undefined'){
            resourceComplementaria.update({id:$scope.complementaria.id}, data, function (data) {
                console.log(data)
                $scope.complementarias = data.list;
                $scope.showFormulario = false;
            }, function (error) {
                console.log(error);
            });
        }else {
            resourceComplementaria.save(data, function (data) {
                console.log(data);
                $scope.complementarias = data.list;
                $scope.showFormulario = false;
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.editarComplementaria = function (id) {
         resourceComplementaria.get({id:id}, function (data) {
            console.log(data);
            $scope.complementaria = data;
            $scope.complementaria.fecha = moment($scope.complementaria.fecha, 'YYYY-MM-DD').format('DD/MM/YYYY');
            $scope.showFormulario = true;
        });
    }

    $scope.showModal = function (id) {
        $scope.complementaria = resourceComplementaria.get({id:id});
        $('#confirmDeleteComplementaria').foundation('reveal', 'open');
    }

    $scope.hideModal = function () {
        $('#confirmDeleteComplementaria').foundation('reveal', 'close');
    }

    $scope.eliminarComplementaria = function (id) {
        resourceComplementaria.delete({id: id}, function(data){
            console.log(data);
            $scope.complementarias = data.list;
        });
        $scope.hideModal();
    }

    /**
     * ACA INICIA LA FUNCIONALIDAD DE ASISTENCIA
     */

     $scope.showAsignarAsistencias = function (asamblea_id) {
         $scope.showAsistencias = true;
         $scope.asamblea = {id:asamblea_id};
         resourceAsistencia = AsistenciasResource.create($('#ambiente_id').val(), asamblea_id);
         resourceAsistencia.query(function (data) {
             $scope.asistencias = data;
             $scope.asistencias.todos = false;
         });
     }

     $scope.cancelarAsistencias = function () {
         $scope.showAsistencias = false;
         $scope.asamblea = {};
     }

     $scope.almacenarAsistencia = function () {
         $scope.showAsistencias = false;

         for (vencedor of $scope.asistencias) {
             if (vencedor.asistencia !== null){
                 vencedor.asistencia.vencedor_id = vencedor.id;
                 if (typeof vencedor.asistencia.id !== 'undefined') {
                     resourceAsistencia.update({id:vencedor.asistencia.id}, vencedor.asistencia, function (data) {
                         console.log(data);
                     }, function (error) {
                         console.log(error);
                     })
                 }else{
                     if (typeof vencedor.asistencia.asistio !== 'undefined') {
                         console.log(vencedor.asistencia);
                         resourceAsistencia.save(vencedor.asistencia, function (data) {
                             console.log(data);
                         }, function (error) {
                             console.log(error);
                         })
                     }
                 }
             }
         }

         $scope.asamblea = {};
     }

     $scope.asistieronTodos = function () {
         var cambio = $scope.asistencias.todos ? '1' : '0';
         for (vencedor of $scope.asistencias) {
             if (vencedor.asistencia === null)
                vencedor.asistencia = {asistio: cambio};
            else {
                vencedor.asistencia.asistio = cambio;
            }
         }
     }

});
