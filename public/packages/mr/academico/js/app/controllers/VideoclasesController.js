angular.module('ambiente')
.controller('VideoclasesController', function ($scope, AsignaturasResource, VideoClasesResource,
                                                VisualizacionVideoclaseResource, AsistenciasResource) {
    resourceVisualizacion = VisualizacionVideoclaseResource.create($('#ambiente_id').val());

    $scope.showFormulario = false; // En inicio el formulario creación/edición permanece oculto
    $scope.showAsistencias = false; // En inicio el formulario de asistencia permanece oculto

    $scope.visualizaciones = resourceVisualizacion.query(); // Se obtiene la lista de videoclases vistas
    $scope.asignaturas = AsignaturasResource.query(); // Lista de asignaturas
    $scope.videoclases = [];

    $scope.cargarVideoclases = function () {
        $scope.videoclases = VideoClasesResource.create($scope.visualizacion.asignatura_id).query();
    }

    // Inicializa el objeto asamblea y activa la bandera de mostar formulario
    $scope.registrarVisualizacionvideo = function () {
        $scope.visualizacion = {};
        $scope.visualizacion.fecha = moment().format('DD/MM/YYYY');
        $scope.showFormulario = true;
    }

    $scope.saveVideoclase = function () {
        data = $scope.visualizacion
        data['fecha'] = moment($scope.visualizacion.fecha, 'DD/MM/YYYY').format('YYYY-MM-DD');

        if( typeof $scope.visualizacion.id !== 'undefined'){
            resourceVisualizacion.update({id:$scope.visualizacion.id}, data, function (data) {
                console.log(data)
                $scope.visualizaciones = data.list;
                $scope.showFormulario = false;
            }, function (error) {
                console.log(error);
            });
        }else {
            resourceVisualizacion.save(data, function (data) {
                console.log(data);
                $scope.visualizaciones = data.list;
                $scope.showFormulario = false;
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.editarVisualizacionvideo = function (id) {
         resourceVisualizacion.get({id:id}, function (data) {
            console.log(data);
            $scope.visualizacion = data;
            $scope.visualizacion.fecha = moment($scope.visualizacion.fecha, 'YYYY-MM-DD').format('DD/MM/YYYY');
            $scope.cargarVideoclases();
            $scope.showFormulario = true;
        });
    }

    $scope.showModal = function (id) {
        $scope.visualizacion = resourceVisualizacion.get({id:id});
        $('#confirmDeleteVisualizacion').foundation('reveal', 'open');
    }

    $scope.hideModal = function () {
        $('#confirmDeleteVisualizacion').foundation('reveal', 'close');
    }

    $scope.eliminarVideoclase = function (id) {
        resourceVisualizacion.delete({id: id}, function(data){
            console.log(data);
            $scope.visualizaciones = data.list;
        });
        $scope.hideModal();
    }

    /**
     * ACA INICIA LA FUNCIONALIDAD DE ASISTENCIA
     */

     $scope.showAsignarAsistencias = function (asamblea_id) {
         $scope.showAsistencias = true;
         $scope.asamblea = {id:asamblea_id};
         resourceAsistencia = AsistenciasResource.create($('#ambiente_id').val(), asamblea_id);
         resourceAsistencia.query(function (data) {
             $scope.asistencias = data;
             $scope.asistencias.todos = false;
         });
     }

     $scope.cancelarAsistencias = function () {
         $scope.showAsistencias = false;
         $scope.asamblea = {};
     }

     $scope.almacenarAsistencia = function () {
         $scope.showAsistencias = false;

         for (vencedor of $scope.asistencias) {
             if (vencedor.asistencia !== null){
                 vencedor.asistencia.vencedor_id = vencedor.id;
                 if (typeof vencedor.asistencia.id !== 'undefined') {
                     resourceAsistencia.update({id:vencedor.asistencia.id}, vencedor.asistencia, function (data) {
                         console.log(data);
                     }, function (error) {
                         console.log(error);
                     })
                 }else{
                     if (typeof vencedor.asistencia.asistio !== 'undefined') {
                         console.log(vencedor.asistencia);
                         resourceAsistencia.save(vencedor.asistencia, function (data) {
                             console.log(data);
                         }, function (error) {
                             console.log(error);
                         })
                     }
                 }
             }
         }

         $scope.asamblea = {};
     }

     $scope.asistieronTodos = function () {
         var cambio = $scope.asistencias.todos ? '1' : '0';
         for (vencedor of $scope.asistencias) {
             if (vencedor.asistencia === null)
                vencedor.asistencia = {asistio: cambio};
            else {
                vencedor.asistencia.asistio = cambio;
            }
         }
     }

});
