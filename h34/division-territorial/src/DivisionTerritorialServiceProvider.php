<?php namespace H34\DivisionTerritorial;

use Illuminate\Support\ServiceProvider;

class DivisionTerritorialServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerDivisionTerritorialInstall();
		$this->registerDivisionTerritorialSeed();
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}


	public function boot()
	{
	    $this->publishes([
			__DIR__ . '/database/migrations/' => base_path('/database/migrations'),
		], 'migrations');

		$this->publishes([
			__DIR__ . '/database/seeds/' => base_path('/database/seeds'),
		], 'seeds');

	}


	public function registerDivisionTerritorialInstall(){
		$this->app->singleton('command.h34.division_territorial.install', function($app){
			return $app['H34\DivisionTerritorial\Console\Commands\DivisionTerritorialInstall'];
		});

		$this->commands('command.h34.division_territorial.install');
	}

	public function registerDivisionTerritorialSeed(){
		$this->app->singleton('command.h34.division_territorial.seed', function($app){
			return $app['H34\DivisionTerritorial\Console\Commands\DivisionTerritorialSeed'];
		});

		$this->commands('command.h34.division_territorial.seed');
	}

}
