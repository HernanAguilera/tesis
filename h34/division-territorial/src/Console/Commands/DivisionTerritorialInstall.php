<?php

namespace H34\DivisionTerritorial\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DivisionTerritorialInstall extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'division-territorial:install';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Crear y ejecutar migraciones del modulo de división territorial.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->call('vendor:publish', ['--provider' => 'H34\DivisionTerritorial\DivisionTerritorialServiceProvider']);
		$this->call('optimize');
		$this->call('migrate');
	}

}
