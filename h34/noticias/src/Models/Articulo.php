<?php namespace H34\CMS\Models;

// use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel as Model;

class Articulo extends Model {

	protected $table = 'noticias_articulos';

	protected $guarded = [];

	public $rules = [
				'titulo' => 'required|string',
				'cuerpo' => 'required|string',
				'categoria_id' => 'exists:noticias_categorias,id',
				'publicado' => 'boolean',
				'destacado' => 'boolean',
				'permitir_comentarios' => 'boolean'
			];


    /**
     * Querys
     */
	public function comentarios(){
		return $this->hasMany('Conexven\Noticias\Models\Comentario');
	}

	public function autor(){
		return $this->belongsTo('Conexven\Usuario');
	}

	public function categoria(){
		return $this->belongsTo('Conexven\Noticias\Models\Categoria');
	}

}
