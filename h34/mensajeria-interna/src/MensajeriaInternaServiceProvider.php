<?php namespace H34\MensajeriaInterna;

use Illuminate\Support\ServiceProvider;

class MensajeriaInternaServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerMensajeriaInternaInstall();
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [
			'Cmgmyr\Messenger\MessengerServiceProvider',
		];
	}

	public function boot(){
		
		include __DIR__.'/routes.php';

		$this->loadViewsFrom(__DIR__.'/resources/views', 'mensajeria-interna');

		$this->publishes([
	        __DIR__.'/resources/views' => base_path('resources/views/h34/mensajeria-interna'),
	    ], 'views');

	    $this->publishes([
			__DIR__ . '/database/migrations/' => base_path('database/migrations'),
		], 'migrations');
	}

	public function registerMensajeriaInternaInstall(){
		$this->app->singleton('command.h34.mensajeria-interna.install', function($app){
			return $app['H34\MensajeriaInterna\Console\Commands\MensajeriaInternaInstall'];
		});

		$this->commands('command.h34.mensajeria-interna.install');
	}

}
