<?php namespace H34\MensajeriaInterna\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MensajeriaInternaInstall extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mensajeria-interna:install';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Crear y ejecutar migraciones del modulo de mensajeria interna.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->call('vendor:publish', ['--provider' => 'H34\MensajeriaInterna\MensajeriaInternaServiceProvider']);
		$this->call('vendor:publish', ['--provider' => 'Cmgmyr\Messenger\MessengerServiceProvider']);
		$this->call('optimize');
		$this->call('migrate');
	}

}
