<?php

Route::controller('messages'
		, 'H34\MensajeriaInterna\Controllers\MessagesController'
		, [
			'getMyMessages' => 'messages.my_messages',
			'getCreate' => 'messages.create',
			'postStore' => 'messages.store',
			'getShow' => 'messages.show',
			'postAddMessage' => 'messages.add'
		  ]);