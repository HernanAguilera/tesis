@extends('body')

@section('content')
    <a href="{{ route('messages.create') }}" class="btn btn-success">Crear nueva conversación</a>
    @if (Session::has('error_message'))
        <div class="alert alert-danger" role="alert">
            {!! Session::get('error_message') !!}
        </div>
    @endif
    @if($threads->count() > 0)
        @foreach($threads as $thread)
        <?php $class = $thread->isUnread($currentUserId) ? 'alert-info' : ''; ?>
        <div class="media alert {!!$class!!}">
            <h4 class="media-heading">
                <a href="{{route('messages.show', $thread->id)}}">{{$thread->subject}}</a>
            </h4>
            <p>{!! $thread->latestMessage->body !!}</p>
            <p>
                <small>
                    <strong>Participants:</strong> 
                    @foreach($thread->participants()->get() as $participant)
                        {{ $participant->user->username }}, 
                    @endforeach
                </small>
            </p>
        </div>
        @endforeach
    @else
        <p>Sorry, no threads.</p>
    @endif
@stop