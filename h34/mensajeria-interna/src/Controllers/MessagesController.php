<?php namespace H34\MensajeriaInterna\Controllers;

use H34\Autenticacion\Models\Usuario as User;
use H34\Core\Controllers\BaseController;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class MessagesController extends BaseController{

	protected $usuario;

	public function __construct(User $usuario){

		$this->usuario = $usuario;

	}

	/**
     * Show all of the message threads to the user
     *
     * @return mixed
     */
	public function getMyMessages(){
		$currentUserId = Auth::user()->id;

		// All threads that user is participating in
        $threads = Thread::forUser($currentUserId)->latest('updated_at')->get();
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages($currentUserId)->latest('updated_at')->get();

        return view('mensajeria-interna::threads.index', compact('threads', 'currentUserId'));
	}

	/**
     * Creates a new message thread
     *
     * @return mixed
     */
    public function getCreate($username)
    {
        $author = Auth::user();
        $recipient = $this->usuario->findByUsername($username);
        if(!$recipient || $recipient->id == Auth::user()->id)
            return redirect()->back()->with('message', 'Usuario no encontrado');
        return view('mensajeria-interna::messages.create', compact('user', 'recipient'));
    }


    /**
     * Stores a new message thread
     *
     * @return mixed
     */
    public function postStore()
    {
        $input = Input::all();
        $thread = Thread::create(
            [
                'subject' => $input['subject'],
            ]
        );
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'body'      => $input['message'],
            ]
        );
        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'last_read' => new Carbon
            ]
        );
        // Recipients
        if (Input::has('recipient')) {
            $thread->addParticipants($input['recipient']);
        }
        return redirect()->route('messages.my_messages');
    }


    /**
     * Shows a message thread
     *
     * @param $id
     * @return mixed
     */
    public function getShow($thread_id)
    {
        try {
            $thread = Thread::findOrFail($thread_id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect('messages.my_messages');
        }

        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();
        // don't show the current user in list
        $userId = Auth::user()->id;
        $thread->markAsRead($userId);
        return view('mensajeria-interna::threads.show', compact('thread'));
    }



    /**
     * Adds a new message to a current thread
     *
     * @param $id
     * @return mixed
     */
    public function postAddMessage($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect('messages');
        }
        $thread->activateAllParticipants();
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::id(),
                'body'      => Input::get('message'),
            ]
        );
        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipants(Input::get('recipients'));
        }
        return redirect()->back();
    }


}