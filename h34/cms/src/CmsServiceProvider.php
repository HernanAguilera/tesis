<?php namespace H34\CMS;

use Illuminate\Support\ServiceProvider;

class CmsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerCmsInstall();
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}


	public function boot()
	{
        include __DIR__.'/routes.php';

		$this->loadViewsFrom(__DIR__.'/resources/views', 'cms');

		$this->publishes([
	        __DIR__.'/resources/views' => base_path('resources/views/h34/cms'),
	    ], 'views');

	    $this->publishes([
			__DIR__ . '/database/migrations/' => base_path('/database/migrations'),
		], 'migrations');

	}

	public function registerCmsInstall(){
		$this->app->singleton('command.h34.cms.install', function($app){
			return $app['H34\CMS\Console\Commands\CmsInstall'];
		});

		$this->commands('command.h34.cms.install');
	}

}
