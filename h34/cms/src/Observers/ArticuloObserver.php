<?php namespace H34\CMS\Observers;

use Illuminate\Support\Str;
use H34\CMS\Models\Articulo;

class ArticuloObserver
{
	function saving($model){
		$model->slug = Str::slug($model->titulo);
	}

}
