<?php namespace H34\CMS\Observers;

use Illuminate\Support\Str;
use H34\CMS\Models\Categoria;

class CategoriaObserver
{	
	function saving($model){
		$model->slug = Str::slug($model->nombre);
	}

}
