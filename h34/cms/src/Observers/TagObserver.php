<?php namespace H34\CMS\Observers;

use Illuminate\Support\Str;
use H34\CMS\Models\Tag;

class TagObserver
{
	function saving($model){
		$model->slug = Str::slug($model->nombre);
	}

}
