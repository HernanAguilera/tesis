<?php namespace H34\CMS\Models;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model {

	protected $table = "cms_comentarios";

	public function autor(){
		return $this->belongsTo('H34\CMS\Models\Usuario');
	}

	public function articulo(){
		return $this->belongsTo('H34\CMS\Models\Articulo', 'cms_articulos', 'articulo_id')
	}

	public function en_respuesta_a(){
		return $this->belongsTo('H34\CMS\Models\Comentario', 'cms_comentarios');
	}

	public function respuestas(){
		return $this->hasMany('H34\CMS\Models\Comentario', 'cms_comentarios', 'comentario_id');
	}

}
