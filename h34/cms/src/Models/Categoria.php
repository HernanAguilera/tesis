<?php namespace H34\CMS\Models;

// use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel as Model;

class Categoria extends Model {

	protected $table = "cms_categorias";

	protected $guarded = [];

	public static $rules = [
		'nombre' => 'string|required',
		'padre_id' => 'exists:cms_categorias,id'
	];

	public function articulos(){
		return $this->hasMany('Articulo', 'cms_articulos');
	}

	public function padre(){
		return $this->belongsTo('H34\CMS\Models\Categoria', 'padre_id', 'id');
	}

	public function hijos(){
		return $this->hasMany('Categoria', 'cms_categorias');
	}

	public static function getList(){
		return self::lists('nombre', 'id');
	}

	public function getValidationRules(){
		return self::$rules;
	}

}
