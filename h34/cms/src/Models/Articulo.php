<?php namespace H34\CMS\Models;

// use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel as Model;

class Articulo extends Model {

	protected $table = 'cms_articulos';

	protected $guarded = [];

	public $rules = [
				'titulo' => 'required|string',
				'cuerpo' => 'required|string',
				'categoria_id' => 'exists:cms_categorias,id',
				'publicado' => 'boolean',
				'destacado' => 'boolean',
				'permitir_comentarios' => 'boolean'
			];


    /**
     * Querys
     */
	public function comentarios(){
		return $this->hasMany('H34\CMS\Models\Comentario');
	}

	public function autor(){
		return $this->belongsTo('H34\Autenticacion\Models\Usuario');
	}

	public function categoria(){
		return $this->belongsTo('H34\CMS\Models\Categoria');
	}

}
