<?php namespace H34\CMS\Models;

// use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel as Model;

class Tag extends Model {

	protected $table = "cms_tags";

	protected $guarded = [];

	public static $rules = [
		'nombre' => 'string|required',
	];

	public function articulos(){
		return $this->belongsToMany('H34\CMS\Models\Articulo', 'cms_articulo_tag', 'tag_id', 'articulo_id');
	}

	public function getValidationRules(){
		return self::$rules;
	}

}
