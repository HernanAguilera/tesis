<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsArticulosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cms_articulos', function(Blueprint $table) {
            $table->increments('id');
            $table->string('titulo',128);
            $table->text('cuerpo');
            $table->boolean('publicado')->default(false);
            $table->string('slug')->nullable();
            $table->boolean('destacado');
            $table->boolean('permitir_comentarios');
            $table->integer('usuario_id')->unsigned()->index()->nullable();
			$table->foreign('usuario_id')->references('id')->on('usuarios')->onDelete('set null');
			$table->integer('categoria_id')->unsigned()->index()->nullable();
			$table->foreign('categoria_id')->references('id')->on('cms_categorias')->onDelete('set null');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cms_articulos');
	}

}
