<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsComentariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cms_comentarios', function(Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->text('cuerpo');
            $table->boolean('publicado');
            $table->integer('usuario_id')->unsigned()->index()->nullable();
			$table->foreign('usuario_id')->references('id')->on('usuarios')->onDelete('set null');
            $table->integer('articulo_id')->unsigned()->index()->nullable();
			$table->foreign('articulo_id')->references('id')->on('cms_articulos')->onDelete('set null');
			$table->integer('comentario_id')->unsigned()->index()->nullable();
			$table->foreign('comentario_id')->references('id')->on('cms_comentarios')->onDelete('set null');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cms_comentarios');
	}

}
