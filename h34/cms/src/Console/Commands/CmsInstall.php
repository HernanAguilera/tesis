<?php namespace H34\CMS\Console\Commands;

use Illuminate\Console\Command;

class CmsInstall extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cms:install';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Crear y ejecutar migraciones del modulo del manejador de contenidos.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->call('vendor:publish', ['--provider' => 'H34\CMS\CmsServiceProvider']);
		$this->call('optimize');
		$this->call('migrate');
	}

}
