@extends('layouts.admin')

@section('work')
    <h1>Permisos</h1>
    <a class="button small success" href="{{route('permisos.create')}}">
        <i class="step fi-plus size-24"></i>
        Agregar un nuevo permiso
    </a>
    @if($permisos->count())
        <table role="grid">
            <thead>
                <tr>
                    <th>
                        Descripción
                    </th>
                    <td class="table-actions">

                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($permisos as $key => $permiso)
                    <tr>
                        <td>
                            {{$permiso->nombre}}
                        </td>
                        <td>
                            <a class="button small secondary action" href="{{route('permisos.show', $permiso->id)}}">
                                <i class="fi-zoom-in"></i>
                                Ver
                            </a>
                            <a class="button small info action" href="{{route('permisos.edit', $permiso->id)}}">
                                <i class="fi-pencil"></i>
                                Editar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert-box">
            No hay permisos registradas
        </div>
    @endif
@stop
