@extends('layouts.admin')

@section('work')

    <h1>Editar permiso</h1>

    {!! Form::model($permiso, ['method' => 'PUT', 'route' => ['permisos.update', $permiso->id]]) !!}

        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', NULL, ['class' => 'form-control']) !!}

        {!! Form::label('roles', 'Roles:') !!}
        {!! Form::select('roles[]', $roles, $permiso_roles, ['multiple' => 'multiple', 'class' => 'chosen-select', 'data-placeholder' => 'Seleccione una opción']) !!}

        <div class="clearfix">
            <a href="{{route('permisos.index')}}" class="button small secondary">
                <i class="fi-list"></i>
                Regresar
            </a>
            <button type="submit" class="button small success">
                <i class="fi-save"></i>
                Guardar
            </button>
            <a href="{{route('permisos.destroy.confirm', $permiso->id)}}" class="button small alert right ">
                <i class="fi-trash"></i>
                Eliminar
            </a>
        </div>

    {!! Form::close() !!}

@stop
