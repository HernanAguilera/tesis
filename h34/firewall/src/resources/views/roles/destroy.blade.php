@extends('layouts.admin')

@section('work')
    <h3 class="page-header">
        <i class="fi-alert"></i>
        Confirma que desea eliminar el registro de id: {{$rol->id}}
        @include('firewall::roles.partials.detail')
    </h3>
    {!! Form::open(['route' => ['roles.destroy', $rol->id], 'method' => 'DELETE']) !!}
        <a class="button small secondary" href="{{route('roles.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <button type="submit" class="button small alert">
            <i class="fi-trash"></i>
            Eliminar
        </button>
    {!! Form::close() !!}
@stop
