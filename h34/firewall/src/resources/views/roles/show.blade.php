@extends('layouts.admin')

@section('work')
    <h1>Detalle de la dimensión</h1>
    @include('firewall::roles.partials.detail')
    <div class="clearfix">
        <a class="button small secondary" href="{{route('roles.index')}}">
            <i class="fi-list"></i>
            Regresar
        </a>
        <a class="button small info" href="{{route('roles.edit', $rol->id)}}">
            <i class="fi-pencil"></i>
            Editar
        </a>
        <a class="button small alert right" href="{{route('roles.destroy.confirm', $rol->id)}}">
            <i class="fi-trash"></i>
            Eliminar
        </a>
    </div>
@stop
