@extends('layouts.admin')

@section('work')
    <h1>Usuarios</h1>
    <a class="button small success" href="{{route('usuarios.create')}}">
        <i class="step fi-plus size-24"></i>
        Agregar una usuario
    </a>
    @if($usuarios->count())
        <table role="grid">
            <thead>
                <tr>
                    <th>
                        Username
                    </th>
                    <th>
                        E-mail
                    </th>
                    <th>
                        Rol
                    </th>
                    <td class="table-actions">

                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($usuarios as $key => $usuario)
                    <tr>
                        <td>
                            {{$usuario->username}}
                        </td>
                        <td>
                            {{$usuario->email}}
                        </td>
                        <td>
                            {{$usuario->rol()}}
                        </td>
                        <td>
                            <a class="button small secondary action" href="{{route('usuarios.show', $usuario->id)}}">
                                <i class="fi-zoom-in"></i>
                                Ver
                            </a>
                            <a class="button small info action" href="{{route('usuarios.edit', $usuario->id)}}">
                                <i class="fi-pencil"></i>
                                Editar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert-box">
            No hay usuarios registradas
        </div>
    @endif
@stop
