<table>
    <tr>
        <th>
            Nombre de usuario
        </th>
        <td>
            {{$usuario->username}}
        </td>
    </tr>
    <tr>
        <th>
            Correo eletrónico
        </th>
        <td>
            {{$usuario->email}}
        </td>
    </tr>
    <tr>
        <th>
            Rol
        </th>
        <td>
            {{$usuario->rol()}}
        </td>
    </tr>
</table>
