<?php namespace H34\Firewall\Console\Commands;

use Illuminate\Console\Command;

class FirewallSeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Data inicial para el componente de autenticacion';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Ejecutando Seeds...');
		$this->call('db:seed', ['--class' => 'FirewallSeeder']);
		$this->info('¡Seeds ejecutados con exito! :-)');
    }
}
