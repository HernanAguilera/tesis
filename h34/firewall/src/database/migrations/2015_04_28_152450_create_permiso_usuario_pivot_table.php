<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermisoUsuarioPivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permiso_usuario', function(Blueprint $table)
		{
			$table->integer('permiso_id')->unsigned()->index();
			$table->foreign('permiso_id')->references('id')->on('permisos')->onDelete('cascade');
			$table->integer('usuario_id')->unsigned()->index();
			$table->foreign('usuario_id')->references('id')->on('usuarios')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permiso_usuario');
	}

}
