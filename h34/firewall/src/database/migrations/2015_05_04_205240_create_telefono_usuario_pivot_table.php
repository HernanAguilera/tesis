<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelefonoUsuarioPivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('telefono_usuario', function(Blueprint $table)
		{
			$table->integer('telefono_id')->unsigned()->index();
			$table->foreign('telefono_id')->references('id')->on('telefonos')->onDelete('cascade');
			$table->integer('usuario_id')->unsigned()->index();
			$table->foreign('usuario_id')->references('id')->on('usuarios')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('telefono_usuario');
	}

}
