<?php namespace H34\Firewall\Controllers;

use H34\Core\Controllers\BaseController as Controller;
use H34\Firewall\Models\Usuario;
use H34\Firewall\Models\Rol;
use H34\Firewall\Requests\UsuarioCreateRequest;
use H34\Firewall\Requests\UsuarioUpdateRequest;

class UsuariosController extends Controller
{
    protected $usuario;
    protected $rol;

    function __construct(Usuario $usuario, Rol $rol){
        $this->usuario = $usuario;
        $this->rol = $rol;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = $this->usuario->all();

        return view('firewall::usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = ['' => '--------'] + $this->rol->lists('nombre', 'id')->toArray();
        return view('firewall::usuarios.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuarioCreateRequest $request)
    {
        $inputs = $request->input();

        $usuario = $this->usuario->create($inputs);
        // if( empty($inputs['rol']) ) echo 'is empty';
        // print_r($inputs);
        // exit();
        if (isset($inputs['rol']) && !empty($inputs['rol'])) {
            $usuario->roles()->sync([$inputs['rol']]);
        }

        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = $this->usuario->findOrFail($id);

        return view('firewall::usuarios.show', compact('usuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = $this->usuario->findOrFail($id);
        $roles = ['' => '--------'] + $this->rol->lists('nombre', 'id')->toArray();
        $usuario_rol = $usuario->roles()->first();
        $usuario_rol_id = $usuario_rol ? $usuario_rol->id : '';
        return view('firewall::usuarios.edit', compact('usuario', 'roles', 'usuario_rol_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsuarioUpdateRequest $request, $id)
    {
        $inputs = $request->input();
        $usuario = $this->usuario->findOrFail($id);
        $usuario->update($inputs);
        
        if (isset($inputs['rol']) && !empty($inputs['rol'])) {
            $usuario->roles()->sync([$inputs['rol']]);
        }

        return redirect()->route('usuarios.index');
    }

    public function destroy_confirm($id)
    {
        $usuario = $this->usuario->findOrFail($id);

        return view('firewall::usuarios.destroy', compact('usuario'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = $this->usuario->findOrFail($id);
        $usuario->delete();

        return redirect()->route('usuarios.index');
    }
}
