<?php namespace H34\Firewall\Controllers;

use H34\Core\Controllers\BaseController as Controller;
use H34\Firewall\Models\Permiso;
use H34\Firewall\Models\Rol;
use H34\Firewall\Requests\PermisoRequest as Request;

class PermisosController extends Controller
{
    protected $permiso;
    protected $rol;

    function __construct(Permiso $permiso, Rol $rol){
        $this->permiso = $permiso;
        $this->rol = $rol;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permisos = $this->permiso->all();

        return view('firewall::permisos.index', compact('permisos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->rol->lists('nombre', 'id');
        return view('firewall::permisos.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        // print_r($inputs);
        // exit();
        $this->permiso->create($inputs)
                      ->roles()
                      ->sync($inputs['roles']);

        return redirect()->route('permisos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permiso = $this->permiso->findOrFail($id);

        return view('firewall::permisos.show', compact('permiso'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permiso = $this->permiso->findOrFail($id);
        $roles = $this->rol->lists('nombre', 'id');
        $permiso_roles = $permiso->roles()->lists('id')->toArray();
        return view('firewall::permisos.edit', compact('permiso', 'roles', 'permiso_roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->input();
        $permiso = $this->permiso->findOrFail($id);
        $permiso->update($inputs)
                ->roles()
                ->sync($inputs['roles']);

        return redirect()->route('permisos.index');
    }

    public function destroy_confirm($id)
    {
        $permiso = $this->permiso->findOrFail($id);

        return view('firewall::permisos.destroy', compact('permiso'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permiso = $this->permiso->findOrFail($id);
        $permiso->delete();

        return redirect()->route('permisos.index');
    }
}
