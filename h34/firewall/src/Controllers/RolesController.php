<?php namespace H34\Firewall\Controllers;

use H34\Core\Controllers\BaseController as Controller;
use H34\Firewall\Models\Rol;
use H34\Firewall\Models\Permiso;
use H34\Firewall\Requests\RolRequest as Request;

class RolesController extends Controller
{
    protected $rol;
    protected $permiso;

    public function __construct(Rol $rol, Permiso $permiso)
    {
        $this->rol = $rol;
        $this->permiso = $permiso;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->rol->all();

        return view('firewall::roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permisos = $this->permiso->lists('nombre', 'id');
        return view('firewall::roles.create', compact('permisos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        // print_r($inputs);
        // exit();
        $rol = $this->rol->create($inputs);
        $rol->permisos()->sync($inputs['permisos']);

        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rol = $this->rol->findOrFail($id);

        return view('firewall::roles.show', compact('rol'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rol = $this->rol->findOrFail($id);
        $permisos = $this->permiso->lists('nombre', 'id');
        $permisos_rol = $rol->permisos()->lists('id')->toArray();
        return view('firewall::roles.edit', compact('rol', 'permisos', 'permisos_rol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rol = $this->rol->findOrFail($id);
        $inputs = $request->input();

        $rol->update($inputs);
        $rol->permisos()->sync($inputs['permisos']);

        return redirect()->route('roles.index');
    }

    public function destroy_confirm($id)
    {
        $rol = $this->rol->findOrFail($id);

        return view('firewall::roles.destroy', compact('rol'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rol = $this->rol->findOrFail($id);
        $rol->delete();

        return redirect()->route('roles.index');
    }
}
