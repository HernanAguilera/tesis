<?php namespace H34\Firewall\Models;

// use SleepingOwl\Models\SleepingOwlModel as Model;
use Illuminate\Database\Eloquent\Model;

class Permiso extends Model {

	protected $table = 'permisos';


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
							'nombre'
					];


	public static $rules = [
		  'nombre' 			=> 'required|alpha_dash'
	];

	public function usuarios(){
		return $this->belongsToMany('\H34\Firewall\Models\Usuario', 'permiso_usuario', 'permiso_id', 'usuario_id');
	}

	public function roles(){
		return $this->belongsToMany('\H34\Firewall\Models\Rol', 'permiso_rol', 'permiso_id', 'rol_id');
	}

	public function getValidationRules(){
        return self::$rules;
	}

    public static function getList(){
        return Permiso::lists('nombre', 'id')->toArray();
    }

}
