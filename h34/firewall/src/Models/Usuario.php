<?php namespace H34\Firewall\Models;

use Illuminate\Auth\Authenticatable;

// use SleepingOwl\Models\SleepingOwlModel as Model;
// use Cmgmyr\Messenger\Traits\Messagable;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class Usuario extends Model implements AuthenticatableContract,
									   CanResetPasswordContract,
									   AuthorizableContract {

    //use Authenticatable, CanResetPassword, Authorizable, Messagable;
	use Authenticatable, CanResetPassword, Authorizable;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuarios';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
							'username'
						  , 'email'
						  , 'password'
						  , 'last_login'
						  , 'locked'
						  , 'expired'
						  , 'expired_at'
						  , 'confirmation_token'
						  , 'password_request_at'
						  , 'verificado'
						  , 'verificado_at'
					];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [
							'password'
						  , 'remember_token'
						  , 'confirmation_token'
						  , 'expired_at'
						  , 'password_request_at'
						  , 'verificado_at'
					];


	public static $rules = [
		'username' 			=> 'required',
		'email' 			=> 'required|email|unique:usuarios',
		'password'          => 'required|alpha_dash|min:6',
        'password-repeat'   => 'required|same:password',
	];


	public function getValidationRules(){
		return self::$rules;
	}


	public function roles(){
		return $this->belongsToMany('\H34\Firewall\Models\Rol', 'rol_usuario', 'usuario_id', 'rol_id');
	}

    public function rol(){
        $rol = $this->roles()->first();
        if ($rol)
            return $rol->nombre;
        return '';
    }

	public function permisos(){
		return $this->belongsToMany('\H34\Firewall\Models\Permiso', 'permiso_usuario', 'usuario_id', 'permiso_id');
	}

	public function perfil(){
		return $this->hasOne('\H34\Firewall\Models\Perfil');
	}

	public function telefonos(){
		return $this->belongsToMany('Telefono', 'telefono_usuario', 'usuario_id'. 'telefono_id');
	}


	public function findByUsername($username){
		return Usuario::where('username', $username)
						->first();
	}

    /**
	 * Lista de permisos de usuario
	 *
	 * @return Array
	 */
    public function getPermisos(){
        return Permiso::join('permiso_rol', 'permisos.id', '=', 'permiso_rol.permiso_id')
              ->join('roles', 'roles.id', '=', 'permiso_rol.rol_id')
			  ->join('rol_usuario', 'roles.id', '=', 'rol_usuario.rol_id')
			  ->join('usuarios', 'usuarios.id', '=', 'rol_usuario.usuario_id')
			  ->select('permisos.nombre')
			  ->distinct()
			  ->where('usuarios.id', $this->id)
			  ->lists('nombre')
              ->toArray();
    }

    /**
	 * Comprueba si el usuario posee un permiso
	 *
	 * @param $permiso String
	 *
	 * @return Boolean
	 */
	public function tienePermiso($permiso){
		return in_array($permiso, $this->getPermisos());
	}



}
