<?php namespace H34\Firewall\Models;

use SleepingOwl\Models\SleepingOwlModel as Model;

class Telefono extends Model {

	protected $table = 'telefonos';


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
                            'tipo',
							'numero'
					];


	public static $rules = [
          'tipo'            => 'required|in:fijo,celular,fax',
		  'numero' 			=> 'required|alpha'
	];


	public function usuarios(){
		return $this->belongsToMany('\H34\Firewall\Models\Usuario', 'telefono_usuario', 'telefono_id', 'usuario_id');
	}


	public function getValidationRules(){
		return self::$rules;
	}

}
