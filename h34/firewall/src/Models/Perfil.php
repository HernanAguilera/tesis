<?php namespace H34\Firewall\Models;

use SleepingOwl\Models\SleepingOwlModel as Model;

class Perfil extends Model {

	protected $table = 'perfiles';


	protected $guarded = [];


	public static $rules = [
		  'usuario_id' 		=> 'required|exits:usuarios,id'
		, 'nombres' 		=> 'alpha'
		, 'apellidos'		=> 'alpha'
		, 'ciudad_id'		=> 'exits:regionalizacion_ciudades,id'
		, 'web'				=> 'url'
		, 'avatar'			=> 'alpha'
	];


	public function usuario(){
		return $this->belongsTo('\H34\Firewall\Models\Usuario');
	}

	public function getValidationRules(){
		return self::$rules;
	}

}
