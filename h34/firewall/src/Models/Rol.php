<?php namespace H34\Firewall\Models;

// use SleepingOwl\Models\SleepingOwlModel as Model;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model {

	protected $table = 'roles';
    public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
							'nombre'
						  , 'description'
					];


	public static $rules = [
		  'nombre' 			=> 'required|alpha'
		, 'descritcion' 		=> 'alpha_dash'
	];


	public function usuarios(){
		return $this->belongsToMany('\H34\Firewall\Models\Usuario', 'rol_usuario', 'rol_id', 'usuario_id');
	}

	public function permisos(){
		return $this->belongsToMany('\H34\Firewall\Models\Permiso', 'permiso_rol', 'rol_id', 'permiso_id');
	}


	public function getValidationRules(){
		return self::$rules;
	}

    public static function getList(){
        return self::lists('nombre', 'id')->toArray();
    }

}
