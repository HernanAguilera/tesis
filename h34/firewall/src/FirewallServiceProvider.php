<?php namespace H34\Firewall;

use Illuminate\Support\ServiceProvider;

use H34\Firewall\Console\Commands\FirewallSeed;

class FirewallServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerFirewallSeed();
	}

	public function boot()
	{
		include __DIR__.'/routes.php';

		$this->loadViewsFrom(__DIR__.'/resources/views', 'firewall');

		$this->publishes([
	        __DIR__.'/resources/views' => base_path('resources/views/firewall'),
	    ], 'views');

	    $this->publishes([
			__DIR__ . '/database/migrations/' => base_path('/database/migrations'),
		], 'migrations');

        $this->publishes([
			__DIR__ . '/database/seeds/' => base_path('/database/seeds'),
		], 'seeds');
	}

    public function registerFirewallSeed(){
        $this->app->singleton('command.h34.firewall.seed', function ($app){
            return $app[FirewallSeed::class];
        });

        $this->commands('command.h34.firewall.seed');
    }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}

}
