<?php namespace H34\Firewall\Requests;

use H34\Core\Requests\Request;

class UsuarioCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' 			=> 'required',
    		'email' 			=> 'required|email|unique:usuarios',
    		'password'          => 'required|alpha_dash|min:6',
            'password-repeat'   => 'required|same:password',
        ];
    }
}
