<?php namespace H34\Firewall\Requests;

use H34\Core\Requests\Request;

class PermisoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'alpha_es'
        ];
    }
}
