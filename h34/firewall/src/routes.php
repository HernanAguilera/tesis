<?php

use H34\Core\Utilidades\Router;

Route::group(['middleware' => ['web']], function () {
    Route::controllers([
        'auth' => 'H34\Firewall\Controllers\AuthController',
        'password' => 'H34\Firewall\Controllers\PasswordController',
    ]);
    Route::group(['prefix' => 'firewall'], function(){
        Router::h34resource('H34\Firewall\Controllers\UsuariosController', 'usuarios');
        Router::h34resource('H34\Firewall\Controllers\RolesController', 'roles');
        Router::h34resource('H34\Firewall\Controllers\PermisosController', 'permisos');
    });
});
