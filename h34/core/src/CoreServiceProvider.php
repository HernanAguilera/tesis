<?php namespace H34\Core;

use Validator;
use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	public function boot(){
		$this->publishes([
	        __DIR__.'/resources/views' => base_path('resources/views/'),
	    ], 'views');

	    $this->publishes([
            __DIR__.'/assets' => public_path('packages/h34/core'),
        ], 'public');

        $this->publishes([
            __DIR__.'/config/menu.xml' => config_path('menu.xml'),
        ]);

        Validator::extend('alpha_es', function($attribute, $value, $parameters, $validator)
        {
            return preg_match("/^([-a-z0-9_-ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöùúûüýøþÿÐdŒ\s-])+$/i", $value);
        });

        Validator::extend('alpha_es_noprint', function($attribute, $value, $parameters)
        {
            return preg_match("/^([-a-z0-9_-ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöùúûüýøþÿÐdŒ\s\W-])+$/i", $value);
        });

        Validator::extend('float', function($attribute, $value, $parameters){
        	return preg_match("/^\d{0,5}\.?\d{0,2}$/", $value);
        });

        Validator::extend('phone', function($attribute, $value, $parameters){
        	return preg_match("/^\d{4}-\d{7}$/", $value);
        }, 'El numero telefonico debe tener el formato 0000-0000000');


	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}

}
