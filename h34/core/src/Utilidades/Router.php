<?php namespace H34\Core\Utilidades;

use Route;

/**
 * Utilidades para rutas
 */
class Router
{

    public static function h34resource($controller, $url){
        $parameter = str_replace('-', '_', $url);
        Route::get($url.'/',                                 $controller.'@index')->name($url.'.index');
        Route::get($url.'/create/',                          $controller.'@create')->name($url.'.create');
        Route::post($url.'/',                                $controller.'@store')->name($url.'.store');
        Route::get($url.'/{'.$parameter.'}/',                      $controller.'@show')->name($url.'.show');
        Route::get($url.'/{'.$parameter.'}/edit/',                 $controller.'@edit')->name($url.'.edit');
        Route::match(['put', 'patch'], $url.'/{'.$parameter.'}/',  $controller.'@update')->name($url.'.update');
        Route::get($url.'/{'.$parameter.'}/destroy',               $controller.'@destroy_confirm')->name($url.'.destroy.confirm');
        Route::delete($url.'/{'.$parameter.'}/',                   $controller.'@destroy')->name($url.'.destroy');
    }
}
