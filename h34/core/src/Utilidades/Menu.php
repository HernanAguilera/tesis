<?php namespace H34\Core\Utilidades;


/**
 * Clase responsable de construir el menú de navegación
 */
class Menu
{

    function __construct(){}

    /*
     *  Contruye el menu para cada usuario
     */
    public static function buildMenu(){
        if(!\Session::has('menu')){
            $menu = [];
            foreach(self::getComponentesId() as $componente_id){
                $funcionalidades = self::getPermisosWithMenuByComponente($componente_id);
                $incluido_componente = false;
                // print_r( \Auth::user()->getPermisos()->toArray() );
                // exit();
                foreach($funcionalidades as $funcionalidad){
                    //\Log::info('Funcionalidad '.$funcionalidad['permiso']);
                    if(in_array($funcionalidad['permiso'], \Auth::user()->getPermisos() ) || \Auth::user()->tienePermiso('es_administrador') ){
                        if(!$incluido_componente){
                            $componente = self::getComponente($componente_id);
                            $menu[(string)$componente_id] = [
                                            'nombre' => (string)$componente[0]['nombre'],
                                            'prefix' => (string)$componente[0]['prefix'],
                                            'route' => (string)$funcionalidad[0]['route'],
                                            'sub-menu' => [
                                                                [
                                                                    'nombre' => (string)$funcionalidad[0]['nombre'],
                                                                    'route' => (string)$funcionalidad[0]['route'],
                                                                    'permiso' => (string)$funcionalidad[0]['permiso']
                                                                ]
                                                        ]
                                            ];
                            $incluido_componente = true;
                        }else{
                            $menu[(string)$componente_id]['sub-menu'][] = [
                                                            'nombre' => (string)$funcionalidad[0]['nombre'],
                                                            'route' => (string)$funcionalidad[0]['route'],
                                                            'permiso' => (string)$funcionalidad[0]['permiso']
                                                        ];
                        }
                    }
                }
            }
            \Session::set('menu', json_encode($menu));
        }
    }

    public static function destroyMenu(){
        \Session::forget('menu');
    }

    /*
     * Retorna los nombres de los componentes de la aplicación
     */
    public static function getComponentesNombre(){
        $app = self::load_file();
        return $app->xpath('//componente/@nombre');
    }

    /*
     * Retorna los IDs de los componentes de la aplicacipión
     */
    public static function getComponentesId(){
        $app = self::load_file();
        return $app->xpath('//componente/@id');
    }

    /*
     * Retorna un componente dado su id
     */
    public static function getComponente($componente_id){
        $app = self::load_file();
        return $app->xpath("//componente[@id='$componente_id']");
    }

    /*
     * Retorna la listas de permisos de cada funcionalidad asociada a un componente pasando
     * el id de este por parametro
     */
    public static function getPermisosByComponente($componente_id){
        $app = self::load_file();
        return $app->xpath("//componente[@id='$componente_id']/funcionalidad");
    }

    /*
     * Retorna los item de lo sub-menus a ser colocados en la barra lateral
     * del panel de administración
     */
    public static function getPermisosWithMenuByComponente($componente_id){
        $app = self::load_file();
        return $app->xpath("//componente[@id='$componente_id']/funcionalidad[@route]");
    }

    /*
     *  Retorna todos los permisos de la aplicación
     */
    public static function getPermisos(){
        $app = self::load_file();
        $permisos = [];
        $componentes = $app->xpath('//componente');
        foreach($componentes as $componente)
            foreach($componente as $funcionalidad)
                $permisos[ (string)$componente['nombre'] ][] = (string)$funcionalidad['permiso'];

        return $permisos;
    }

    protected static function load_file(){
        return simplexml_load_file(config_path('menu.xml'));
    }
}
