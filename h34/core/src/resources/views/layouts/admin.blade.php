@extends('layouts.body')

@section('main-menu')
    @if(\Session::has('menu'))
	@foreach(json_decode(\Session::get('menu', []), true) as $item)
	    <li
        @if(\Request::is( preg_replace('/^\//i', '', route($item['route'], [], false)) ) || \Request::is( preg_replace('/^\//i', '', $item['prefix']).'/*' ))
		    class = 'active'
		@endif
	    >
		<a href="{{ route($item['route']) }}">{{$item['nombre']}}</a>
	    </li>
	@endforeach
    @endif
@stop

@section('content')
    <div class="columns medium-2 sidebar">
        <span class="class="thumbnail"">
            <img src="/images/misionribas_250x197b.jpg" alt="logo misión ribas" />
        </span>
        <ul class="side-nav menu">
            @if(\Session::has('menu'))
			    @foreach(json_decode(\Session::get('menu', []), true) as $item)
					@if(\Request::is( preg_replace( '/^\//i', '', route($item['route'], [], false) )) || \Request::is( preg_replace( '/^\//i', '', $item['prefix'] ).'/*') )
					    @foreach($item['sub-menu'] as $submenu)
					    	@if( \Request::is( preg_replace('/^\//i', '', route($submenu['route'], [], false)) ) || \Request::is( preg_replace( '/^\//i', '', route($submenu['route'], [], false) ).'/*') )
                                <li class="active">
                                    <a href="{{ route($submenu['route']) }}">
                                        {{ $submenu['nombre'] }}
                                    </a>
                                </li>
					    	@else
                                <li>
                                    <a href="{{ route($submenu['route']) }}">
                                        {{ $submenu['nombre'] }}
                                    </a>
                                </li>
					    	@endif
					    @endforeach
					@endif
			    @endforeach
			@endif
        </ul>
    </div>
    <div class="columns medium-10 medium-offset-2 main">
        @include('layouts.fragments.messages')
        @include('layouts.fragments.errors')
        @yield('work')
    </div>
@stop
